import { Component, OnInit, OnDestroy, ElementRef, Input, Output, EventEmitter, AfterContentInit, AfterViewInit } from '@angular/core';
import { VideoEventsService } from '../services/video-events.service'
declare var $: any;
import videojs from 'video.js'

@Component({
  selector: 'videojs',
  template: `
  <video *ngIf="url" id="video_{{idx}}"
     class="video-js vjs-default-skin vjs-big-play-centered vjs-16-9"
     controls preload="auto">
    <source [src]="url" type="video/mp4" />
  </video>
  `,
})
export class VideojsComponent implements OnInit, AfterViewInit {
  videoJsPlayer: any; 
  @Input() idx: string;
  @Input() url: any;

  @Output() videoPlaying = new EventEmitter<boolean>();
  @Output() videoPaused = new EventEmitter<boolean>();

  constructor(private videoEventsService: VideoEventsService) { 
    this.url = false;
  }

  ngOnInit() {
    this.videoEventsService.$videoPlayEvents.subscribe(video => {
      //this.url = video.videoUrl;
      //this.videoJsPlay();
    })
    this.videoEventsService.$videoPauseEvents.subscribe(ev => {
      this.videoJsPause();
    })
  }

  ngAfterViewInit() {
    let element = 'video_' + this.idx;
    let self = this;
    videojs(document.getElementById(element)).ready(function() {
      try {
      let videoJsPlayer = this;
      videoJsPlayer.play();
      
      videoJsPlayer.on('play', () => {
        self.videoPlaying.emit(true);
        self.videoPaused.emit(false);
      });  
      videoJsPlayer.on('pause', () => {
        self.videoPlaying.emit(false);
        self.videoPaused.emit(true);
      });  
      } catch (error) {
        console.log('video js error')
      }
    });
    
  }
  videoJsPlay() {
    this.videoJsPlayer.play();
  }
  videoJsPause() {
    this.videoJsPlayer.pause();
  }
}
