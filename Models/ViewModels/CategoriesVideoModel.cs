﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class CategoriesVideoModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
