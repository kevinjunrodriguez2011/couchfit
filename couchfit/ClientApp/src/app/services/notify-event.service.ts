import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject'
import { VideoModel } from '../models/video-model'
import { VideosCategoryModel } from '../models/videoscategory-model'
import { Subscription } from 'rxjs';
import { PlanTypeModel } from '../models/payment/plan-type.model';

@Injectable({
  providedIn: 'root'
})
export class NotifyEventService {

  private planSelectionEvent = new Subject<any>();
  $planSelectionEvent = this.planSelectionEvent.asObservable();

  constructor() { }

  public onPlanSelection(planTypeModel:PlanTypeModel) {
    debugger;
    this.planSelectionEvent.next(planTypeModel);
  }
}
