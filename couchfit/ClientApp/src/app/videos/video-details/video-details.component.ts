import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { VideoEventsService } from '../../services/video-events.service'
import { VideoModel } from '../../models/video-model'

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.css'],
})
export class VideoDetailsComponent implements OnInit {

  videoModel = new VideoModel();
  videoLoaded: boolean = false;
  showPlayBtn = true;
  showPauseBtn = false;
  videoEaseIn = false;

  constructor(private videoEventsService: VideoEventsService,
    private ref: ChangeDetectorRef) {
    this.videoEventsService.$videoSelectDetailsEvents.subscribe(video => {
      this.videoEaseIn = true;
      this.videoModel = video;
      this.videoLoaded = true;
      this.showPlayBtn = true;
      this.showPauseBtn = false;
      //ref.detectChanges();
      setTimeout(() => {
        this.videoEaseIn = false;
      }, 500)
    })
  }

  ngOnInit() {

  }
  showPlay() {
    this.showPlayBtn = !this.showPlayBtn
    if (this.showPlayBtn) {
      this.showPauseBtn = false;
    }
    else {
      this.showPauseBtn = true;
      this.videoEventsService.onVideoPlay(this.videoModel);
    }
  }
  showPause() {
    this.showPauseBtn = !this.showPauseBtn;
    if (this.showPauseBtn) {
      this.showPlayBtn = false;
    }
    else {
      this.showPlayBtn = true;
      this.videoEventsService.onVideoPause();
    }
  }
}
