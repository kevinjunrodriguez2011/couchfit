import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StripeReturnMessageComponent } from './stripe-return-message.component';

describe('StripeReturnMessageComponent', () => {
  let component: StripeReturnMessageComponent;
  let fixture: ComponentFixture<StripeReturnMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StripeReturnMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StripeReturnMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
