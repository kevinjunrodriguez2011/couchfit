import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';
import { LoginModel } from '../models/login-model';
import { RegisterModel } from '../models/register-model';
import { Subject } from 'rxjs/Subject';
import { UserDetailsService } from '../services/user-details.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private router: Router, private userDetailsService: UserDetailsService) { }
  
  isAuthenticated = false;
  private facebookLoginEvents = new Subject<any>();
  facebookLoginEventsSource$ = this.facebookLoginEvents.asObservable();

  private googleLoginEvents = new Subject<any>();
  googleLoginEventsSource$ = this.googleLoginEvents.asObservable();

  onFacebookLogin() {
    this.facebookLoginEvents.next('');
  } 
  onGoogleLogin() {
    this.googleLoginEvents.next('');
  }
  login(loginModel: LoginModel) {
    let  body = new HttpParams()
    .set('email', loginModel.Email)
    .set('password', loginModel.Password);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    return this.httpClient.post<any>('api/auth/login', body.toString(), httpOptions);
  }
  register(registerModel: RegisterModel) {
    let body = new HttpParams()
    .set('Email', registerModel.Email)
    .set('Password', registerModel.Password);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    return this.httpClient.post<any>('api/auth/register', body.toString(), httpOptions);
  }
  logOut() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }
  confirmEmail(userId: string) {
    let body = new HttpParams()
    .set('userId', userId)

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    return this.httpClient.post<any>('api/auth/confirmemail', body.toString(), httpOptions);
  }
  authenticate() {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.httpClient.get<any>('api/auth/validatetoken?token=' + token, httpOptions);
  }
  facebookLogin(accessToken:string) {
    let headers = new Headers();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    let body = new HttpParams()
    .set('accessToken', accessToken) 
    return this.httpClient.post<any>('api/auth/facebooklogin', body.toString(), httpOptions);
  }
  googleLogin(code: string) {
    let headers = new Headers();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    let body = new HttpParams()
    .set('code', code) 
    return this.httpClient.post<any>('api/auth/googlelogin', body.toString(), httpOptions);
  }
}
