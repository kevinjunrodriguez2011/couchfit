﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class StripeServiceBase
    {
        private static bool isDev = true;

        public StripeServiceBase()
        {
            if (isDev)
                StripeConfiguration.SetApiKey("sk_test_WZD4qSY5vaG8XZ1Pdl8RJDor");
            else
                StripeConfiguration.SetApiKey("");

        }
        public StripeCustomerService GetCustomerService()
        {
            return new StripeCustomerService();
        }
        public StripePlanService GetStripePlanService()
        {
            return new StripePlanService();
        }
        public StripeSubscriptionService GetSubscriptionService()
        {
            return new StripeSubscriptionService();
        }
        public StripeSubscriptionItemService GetSubscriptionItemService()
        {
            return new StripeSubscriptionItemService();
        }
    }
}
