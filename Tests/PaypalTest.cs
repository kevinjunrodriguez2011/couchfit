﻿using System;
using Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class PaypalTest
    {
        private readonly IPaypalService _paypalService;
        public PaypalTest(IPaypalService paypalService)
        {
            _paypalService = paypalService;
        }
        [TestMethod]
        public void PaypalInitiateTest()
        {
            _paypalService.PaypalInitiate();
        }

    }
}
