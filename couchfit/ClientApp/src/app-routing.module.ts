import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router'
import { AuthGuard } from './app/auth.guard';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: 'app/landing-page/landing-page.module#LandingPageModule'
  },
  {
    path: 'videos',
    loadChildren: 'app/videos/videos.module#VideosModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'player',
    loadChildren: 'app/player/player.module#PlayerModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'auth',
    loadChildren: 'app/auth/auth.module#AuthModule',
  },
  {
    path: 'onboard',
    loadChildren: 'app/onboard/onboard.module#OnboardModule',
  },
  {
    path: 'payment',
    loadChildren: 'app/payment/payment.module#PaymentModule'
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }
];


@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ]
})
export class AppRoutingModule { }
