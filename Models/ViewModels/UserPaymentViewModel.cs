﻿using Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class UserPaymentViewModel
    {
        public string PaypalPaymentToken { get; set; }
        public string AgreementId { get; set; }
        public string UserId { get; set; }
        public string CancelledReason { get; set; }
        public string SuspendedReason { get; set; }
        public SubscriptionPlansModel SubscriptionPlansModel { get; set; }
    }
}
