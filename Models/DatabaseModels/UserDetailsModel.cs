﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.DatabaseModels
{
    public class UserDetailsModel
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
    }
}
