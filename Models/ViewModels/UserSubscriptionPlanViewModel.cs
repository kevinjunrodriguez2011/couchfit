﻿using Models.DatabaseModels;
using Models.PaymentModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class UserSubscriptionPlanViewModel
    {
        public SubscriptionPlansModel subscriptionPlansModel { get; set; }
        public string SubscriptionStatus { get; set; }
    }
}
