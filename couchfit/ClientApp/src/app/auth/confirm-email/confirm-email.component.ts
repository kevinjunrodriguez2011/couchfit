import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private activatedRoute: ActivatedRoute) { 
    this.activatedRoute.queryParams.subscribe(params => {
      let userId = params['userId'];
      this.authService.confirmEmail(userId).subscribe(res => {
        if (res.message === 'Confirmed') {
          localStorage.setItem('token', res.token);
          localStorage.removeItem('confirmemail');
          this.router.navigate(['/onboard/welcome'])
        }
      })
    });
  }

  ngOnInit() {
    
  }
}
