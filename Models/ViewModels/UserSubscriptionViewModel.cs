﻿using Models.DatabaseModels;
using Models.PaymentModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class UserSubscriptionViewModel
    {
        public UserSubscriptionModel userSubscriptionModel { get; set; }
        public PlanType planType { get; set; }
    }
}
