import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }
  getUserDetails() {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/user/getuserdetails?token=' + token, httpOptions)
  }
  updateUserDetails(parameter: object) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    var jsonParam = JSON.stringify(parameter);
    let body = new HttpParams()
    .set('parameter', jsonParam) 
    return this.httpClient.post<any>('api/user/updateuserdetails', body.toString(), httpOptions)
  }
  skipIntroPayment(userId: string) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    let body = new HttpParams()
    .set('userId', userId) 
    return this.httpClient.post<any>('api/user/skipintropayment', body.toString(), httpOptions)
  }
}
