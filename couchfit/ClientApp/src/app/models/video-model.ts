export class VideoModel {
    videoId: number;
    videoName: string;
    videoDescription: string;
    videoUrl: string;
    videoThumbnail: string;
    categoryId: number;
    categoryName: string;
}