import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { PlayerComponent } from './player/player.component';
import { PlayerRoutingModule } from './/player-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    PlayerRoutingModule
  ],
  declarations: [
    PlayerComponent, 
  ],
  exports: [PlayerComponent]
})
export class PlayerModule { }
