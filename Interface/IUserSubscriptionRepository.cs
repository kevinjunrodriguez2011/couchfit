﻿using Models.DatabaseModels;
using Models.ViewModels;

namespace Interface
{
    public interface IUserSubscriptionRepository
    {
        UserSubscriptionPlanViewModel GetUserSubscriptionPlan(string userId);
        int SaveUserSubscription(Models.ViewModels.UserSubscriptionViewModel userSubscription);
        int UpdateUserSubscription(Models.ViewModels.UserSubscriptionViewModel userSubscription);
    }
}
