import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../../models/login-model';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private userService: UserService) { }

  loginModel: LoginModel;
  errors = [];
  hasError = false;
  logginIn = false;

  ngOnInit() {
    this.loginModel = new LoginModel();
  }
  login() {
    this.logginIn = true;
    if ((this.loginModel.Email !== undefined && this.loginModel.Email.trim() !== '') && (this.loginModel.Password !== undefined && this.loginModel.Password.trim() !== '')) {
      this.authService.login(this.loginModel).subscribe(res => {
        if (!res.hasError) {
          localStorage.setItem('token', res.token);
          this.router.navigate(["/onboard/welcome"]);
        }
        if (res.hasError) {
          this.errors = [];
          this.hasError = true;
          this.errors.push({'description' : res.message});

          if (res.emailUnconfirmed !== undefined && res.emailUnconfirmed) {
            this.router.navigate(['/confirmemail'])
          }
          if (res.direct !== undefined && !res.direct) {
            this.errors.push({'description' : "Email or password is incorrect."});
          }
          if (res.SystemError) {
            this.errors.push({'description' : "Login is unavailable right now, please try again later"});
          }
          this.loginModel.Password = '';
        }
      })
    }
  }
  openFbLogin() {
    this.authService.onFacebookLogin();
  }
  openGoogleLogin() {
    this.authService.onGoogleLogin();
  }
}
  