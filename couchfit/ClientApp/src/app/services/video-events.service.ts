import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject'
import { VideoModel } from '../models/video-model'
import { VideosCategoryModel } from '../models/videoscategory-model'
import { Subscription } from 'rxjs';

@Injectable()
export class VideoEventsService {

  private videoPlayEventSource = new Subject<any>();
  private videoPauseEventSource = new Subject<any>();
  private videoSelectDetailsEventSource = new Subject<any>();
  private videoSelectCategorySource = new Subject<any>();
  private videoSelectAllSource = new Subject<any>();
  private videoPlayAllSource = new Subject<any>();
  private playerLoadedSource = new Subject<any>();
  private allVideosModalSource = new Subject<any>();
  public currentPlayerVideo: any;
  public allVideosEventSubscription: Subscription;
  
  constructor() { }

  $videoPlayEvents = this.videoPlayEventSource.asObservable();
  $videoPauseEvents = this.videoPauseEventSource.asObservable();
  $videoSelectDetailsEvents = this.videoSelectDetailsEventSource.asObservable();
  $videoSelectCategoryEvents = this.videoSelectCategorySource.asObservable();
  $videoSelectAllEvents = this.videoSelectAllSource.asObservable();
  $videoPlayAllEvents = this.videoPlayAllSource.asObservable();
  $playerLoadedEvents = this.playerLoadedSource.asObservable();
  $allVideosModalEvents = this.allVideosModalSource.asObservable();

  onVideoPlay(video: VideoModel) {
    this.videoPlayEventSource.next(video);
  }
  onVideoPause() {
    this.videoPauseEventSource.next('');
  }
  onVideoSelectDetails(video) {
    this.videoSelectDetailsEventSource.next(video);
  }
  onVideoSelectCategory(videoCategoryId: number) {
    this.videoSelectCategorySource.next(videoCategoryId);
  }
  onVideoSelectAll() {
    this.videoSelectAllSource.next();
  }
  onVideoPlayAll(video) {
    this.videoPlayAllSource.next(video);
  }
  onPlayerLoaded() {
    this.playerLoadedSource.next();
  }
  onShowAllVideosModal() {
    this.allVideosModalSource.next();
  }
}
