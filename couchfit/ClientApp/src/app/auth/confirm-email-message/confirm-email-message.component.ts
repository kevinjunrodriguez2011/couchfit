import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm-email-message',
  templateUrl: './confirm-email-message.component.html',
  styleUrls: ['./confirm-email-message.component.css']
})
export class ConfirmEmailMessageComponent implements OnInit {

  constructor() { 
    let confirmEmail = localStorage.getItem('confirmemail');
    if (!confirmEmail) {
      window.location.href = "/main";
    }
  }

  ngOnInit() {
  }

}
