import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaypalReturnComponent } from './paypal-return/paypal-return.component';
import { PaypalCancelComponent } from './paypal-cancel/paypal-cancel.component';
import { PaypalReturnMessageComponent } from './paypal-return-message/paypal-return-message.component';
import { StripeReturnMessageComponent } from './stripe-return-message/stripe-return-message.component';

const routes: Routes = [
  {
    path: 'paypal/return-message',
    component: PaypalReturnMessageComponent
  },
  {
    path: 'paypal/return',
    component: PaypalReturnComponent
  },
  {
    path: 'paypal/cancel',
    component: PaypalCancelComponent
  },
  {
    path: 'stripe/return-message',
    component: StripeReturnMessageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
