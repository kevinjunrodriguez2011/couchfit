import { Injectable } from '@angular/core';
import { StripeSubscriptionViewModel } from '../models/ViewModels/StripeSubscriptionViewModel';

@Injectable()
export class StripeService {

   stripeSubscriptionViewModel = new StripeSubscriptionViewModel();

   constructor() {

   }
   getStripeInstance() {
    let stripe = Stripe('pk_test_2fYUf3obgBuz7h6FjSS3rxxu');;
    return stripe
   }
   getStripSubscriptionDetails() {
    if (this.stripeSubscriptionViewModel) {
      return this.stripeSubscriptionViewModel;
    }
   }
   setStripeSubscriptionDetails(stripeSubscriptionViewModel: StripeSubscriptionViewModel) {
    this.stripeSubscriptionViewModel.StripeToken = stripeSubscriptionViewModel.StripeToken;
    this.stripeSubscriptionViewModel.FirstName = stripeSubscriptionViewModel.FirstName;
    this.stripeSubscriptionViewModel.CreditCardNumber = stripeSubscriptionViewModel.CreditCardNumber;
    this.stripeSubscriptionViewModel.CreditExpiry = stripeSubscriptionViewModel.CreditExpiry;
    //this.stripeSubscriptionViewModel.CVC = stripeSubscriptionViewModel.CVC;
   }
}
