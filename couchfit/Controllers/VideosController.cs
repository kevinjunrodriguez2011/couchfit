﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace couchfit_ng.Controllers
{
    
    [Authorize(Policy = "User")]
    public class VideosController : Controller
    {
        private readonly IVideosRepository _iVideosRepository;
        public VideosController(IVideosRepository iVideosRepository)
        {
            _iVideosRepository = iVideosRepository;
        }
        public object GetVideos()
        {
            var videos = _iVideosRepository.getVideos();
            return videos;
        }
        public object GetVideosCategory()
        {
            var videosCategory = _iVideosRepository.getVideosCategories();
            return videosCategory;
        }
        public object GetVideosByCategory(int categoryId)
        {
            var videos = _iVideosRepository.getVideosByCategory(categoryId);
            return videos;
        }
        public object GetVideosById(int videoId)
        {
            var video = _iVideosRepository.getVideosById(videoId);
            return video;
        }
    }
}