import { PlanTypeModel } from "./plan-type.model";

export class PayerModel {
    UserId: string;
    FirstName: string;
    LastName: string;
    Email: string;
    ChosenPlan: PlanTypeModel;
}   