import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartupService } from './/startup.service';

export function startUp(startUpService: StartupService) {
  return () => startUpService.load();
}

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [StartupService, { provide: APP_INITIALIZER, useFactory: startUp, deps: [StartupService], multi: true }]
})
export class StartupModule { }
