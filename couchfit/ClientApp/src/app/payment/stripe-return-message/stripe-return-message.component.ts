import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-stripe-return-message',
  templateUrl: './stripe-return-message.component.html',
  styleUrls: ['./stripe-return-message.component.css'],
  animations: [
    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class StripeReturnMessageComponent implements OnInit {

  constructor(private router: Router) {
    var isReturnSuccess = localStorage.getItem('returnSuccess')
    if (isReturnSuccess == undefined) {
      window.location.href = "/main";
    }
   }

  ngOnInit() {
  }
  browseVideos() {
    this.router.navigate(['/videos']);
    localStorage.removeItem('returnSuccess');
  }
}
