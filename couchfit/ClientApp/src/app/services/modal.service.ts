import { Injectable, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalRef: BsModalRef;
  config = {
    animated: true,
    ignoreBackdropClick: true
  };
  constructor(private modalService: BsModalService) {

   }
   openModal(template: TemplateRef<any>) {
    return this.modalService.show(template, this.config);
  }
}
