import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';
import { UserService } from '../../services/user.service';
import { UserDetailsService } from '../../services/user-details.service';
import { NotifyEventService } from '../../services/notify-event.service';
import { PlanTypeModel } from '../../models/payment/plan-type.model';
import { PaymentService } from '../../services/payment.service';
import { PayerModel } from '../../models/payment/payer.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { takeUntil, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { SharedService } from '../../services/shared.service';
import { StripeService } from '../../services/stripe.service';
import { StripeSubscriptionViewModel } from '../../models/ViewModels/StripeSubscriptionViewModel';

@Component({
  selector: 'app-intro-payment',
  templateUrl: './intro-payment.component.html',
  styleUrls: ['./intro-payment.component.css'],
  animations: [
    trigger('slidePay', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms', style({transform: 'translateX(100%)'}))
      ])
    ]),

    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class IntroPaymentComponent implements OnInit, AfterViewInit {
  paymentSelected = false;
  selectedPlan : PlanTypeModel;
  currentUser: any;
  paymentSubscription: Subscription;
  ccFirstName: string;
  ccLastName: string;

  @ViewChildren('cardElement') cardElement : ElementRef;
  constructor(private userService: UserService, 
    private notifyEventService: NotifyEventService, 
    private router: Router, 
    private paymentService: PaymentService,
    private spinnerService: NgxSpinnerService,
    private sharedService: SharedService,
    private stripeService: StripeService,
    private elRef:ElementRef) { 
    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan')) as PlanTypeModel;
  }

  ngOnInit() {
  }
  payWithPaypal() {
    this.spinnerService.show();
    this.router.navigate(['onboard/payment-confirm/paypal'])
  }
  cancelPaypalRedirect() {
    this.spinnerService.hide();
    this.paymentSubscription.unsubscribe();
  }
  showCreditCardForm() {
    this.paymentSelected = true;
    setTimeout(() => {
      let stripe = this.stripeService.getStripeInstance();
      var elements = stripe.elements();

      var elementStyles = {
        base: {
          color: '#495057',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: 'normal',
          fontSmoothing: 'antialiased', 
          ':focus': {
            color: '#424770',
          },
          '::placeholder': {
            color: '#7e7f80',
          },
    
        },
        invalid: {
          color: 'red',
          ':focus': {
            color: '#FA755A',
          },
          '::placeholder': {
            color: '#FFCCA5',
          },
        },
      };
    
      var elementClasses = {
        focus: 'focus',
        empty: 'empty',
        invalid: 'invalid',
      };
      var cardNumber = elements.create('cardNumber', {
        style: elementStyles,
        classes: elementClasses,
      });
      cardNumber.mount('#card-number');
    
      var cardExpiry =  elements.create('cardExpiry', {
        style: elementStyles,
        classes: elementClasses,
      });
      cardExpiry.mount('#card-expiry');
    
      var cardCvc =  elements.create('cardCvc', {
        style: elementStyles,
        classes: elementClasses,
      });
      cardCvc.mount('#card-cvc');
      // Handle real-time validation errors from the card Element.
      cardNumber.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-number-errors');
        if (event.brand) {
          setBrandIcon(event.brand);
        }
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });
      cardExpiry.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-expiry-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });
      cardCvc.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-cvc-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });
      var cardBrandToPfClass = {
        'visa': 'pf-visa',
        'mastercard': 'pf-mastercard',
        'amex': 'pf-american-express',
        'discover': 'pf-discover',
        'diners': 'pf-diners',
        'jcb': 'pf-jcb',
        'unknown': 'pf-credit-card',
      }
      
      function setBrandIcon(brand) {
        var brandIconElement = document.getElementById('brand-icon');
        var pfClass = 'pf-credit-card';
        if (brand in cardBrandToPfClass) {
          pfClass = cardBrandToPfClass[brand];
        }
        for (var i = brandIconElement.classList.length - 1; i >= 0; i--) {
          brandIconElement.classList.remove(brandIconElement.classList[i]);
        }
        brandIconElement.classList.add('pf');
        brandIconElement.classList.add(pfClass);
      }
      var stripeForm = document.getElementById('stripe-payment-form');
      var stripeSubscriptionViewModel = new StripeSubscriptionViewModel();
      let that = this;
      stripeForm.addEventListener('submit', function(event) {
      event.preventDefault();
      stripe.createToken(cardNumber).then(function(result) {
        if (result.error) {
          // Inform the user if there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.textContent = result.error.message;
        } else {
          that.spinnerService.show();
          var card = result.token.card;
          stripeSubscriptionViewModel.StripeToken = result.token.id;
          stripeSubscriptionViewModel.FirstName = that.ccFirstName + ' ' + that.ccLastName;
          stripeSubscriptionViewModel.CreditCardNumber = "XXXX XXXX XXXX" + card.last4;
          stripeSubscriptionViewModel.CreditExpiry = card.exp_month + '/' + card.exp_year;
          that.stripeService.setStripeSubscriptionDetails(stripeSubscriptionViewModel);
          that.router.navigate(['onboard/payment-confirm/cc'])
          // Send the token to your server.
          //stripeTokenHandler(result.token);
        }
      });
    });
    }, 10)
  }

  hideForm() {
    this.paymentSelected = false;
  }
  changePlan() {
    this.router.navigate(['/onboard/plans'])
  }
  skipIntroPayment() {
    this.spinnerService.show();
    this.sharedService.skipIntroPayment();
  }
  ngAfterViewInit() {

  }
}