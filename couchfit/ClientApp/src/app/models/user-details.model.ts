export class UserDetailsModel {
    UserId: string;
    FirstName: string;
    LastName: string;
    Gender: string;
}