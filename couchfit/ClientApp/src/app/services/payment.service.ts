import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import { takeUntil, map } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';
import { PayerModel } from '../models/payment/payer.model';
import { UserPaymentViewModel } from '../models/ViewModels/UserPaymentViewModel';
import { PlanTypeModel } from '../models/payment/plan-type.model';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private httpClient: HttpClient) { 

  }
  paypalCreateBillingAgreement(payer: PayerModel) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    var jsonParam = JSON.stringify(payer);
    let body = new HttpParams()
    .set('parameter', jsonParam) 
    return this.httpClient.post<any>('api/paypal/CreateBillingAgreement', body.toString(), httpOptions)
  } 
  paypalExecuteBillingAgremeent(userPaymentViewModel: UserPaymentViewModel) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    let body = new HttpParams()
    .set('parameter', JSON.stringify(userPaymentViewModel)) 
    return this.httpClient.post<any>('api/paypal/ExecuteBillingAgreement', body.toString(), httpOptions)
  }
  paypalCancelBillingAgreement(userPaymentViewModel: UserPaymentViewModel) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    let body = new HttpParams()
    .set('parameter', JSON.stringify(userPaymentViewModel)) 
    return this.httpClient.post<any>('api/paypal/CancelBillingAgreement', body.toString(), httpOptions)
  }
  paypalSuspendBillingAgreement(userPaymentViewModel: UserPaymentViewModel) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    let body = new HttpParams()
    .set('parameter', JSON.stringify(userPaymentViewModel))
    return this.httpClient.post<any>('api/paypal/SuspendBillingAgreement', body.toString(), httpOptions)
  }
  paypalReactivateBillingAgreement(userPaymentViewModel: UserPaymentViewModel) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    let body = new HttpParams()
    .set('parameter', JSON.stringify(userPaymentViewModel))
    return this.httpClient.post<any>('api/paypal/ReactivateBillingAgreement', body.toString(), httpOptions)
  }
  stripeCreateSubscription(stripeToken: string, planType: PlanTypeModel, userId: string) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    var jsonParam = JSON.stringify({
      token: stripeToken,
      planName: planType.PlanName,
      planPrice: planType.PlanPrice,
      userId: userId
    });
    let body = new HttpParams()
    .set('parameter', jsonParam) 
    return this.httpClient.post<any>('api/stripe/CreateSubscription', body.toString(), httpOptions)
  }
}
