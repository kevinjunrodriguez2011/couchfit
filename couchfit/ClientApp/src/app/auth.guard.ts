import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private userService: UserService, private authService: AuthService,
    private router: Router){
      
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.authService.authenticate().map(isAuth => {
        if (isAuth) {
          this.authService.isAuthenticated = true;
          this.userService.getUserDetails().map(user => {
            if (user.firstName === null && user.lastName === null && user.gender === null) {
              window.location.href = '/onboard/welcome';
            } 
            if (user.skippedPaymentIntro === null) {
              window.location.href = '/onboard/plans';
            }
          })
          return true;
        }
        this.router.navigate(['/login'])
      })
  }
}
