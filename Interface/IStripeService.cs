﻿using System;
using System.Collections.Generic;
using System.Text;
using Models.PaymentModels;
using Models.PaymentModels.StripeModel;
using Stripe;

namespace Interface
{
    public interface IStripeService
    {
        StripeCustomer CreateCustomer(string token);
        StripeCustomer RetrieveCustomer(string cus_id);
        StripeCustomer UpdateStripeCustomer(string cus_id);
        StripeDeleted DeleteCustomer(string cus_id);
        StripeList<StripeCustomer> GetAllCustomers();
        StripePlan CreatePlan(PlanType planType);
        StripePlan RetrievePlan(string planId);
        StripePlan UpdatePlan(string planId, StripePlanUpdateOptions stripePlanUpdateOptions);
        StripeDeleted DeletePlan(string planId);
        StripeSubscription CreateSubscription(string cus_id, string plan_id);
        StripeSubscriptionItem RetrieveSubscriptionItem(string sub_id);
        StripeSubscriptionItem UpdateSubscriptionItem(string sub_id, string plan_id, int quantity);
        StripeDeleted DeleteSubscriptionItem(string sub_id);

    }
}
