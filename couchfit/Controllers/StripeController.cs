﻿using Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.DatabaseModels;
using Models.PaymentModels;
using Models.ViewModels;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;

namespace couchfit_ng.Controllers
{
    [Authorize(Policy = "User")]
    public class StripeController : Controller
    {
        private readonly IStripeService _stripeService;
        private readonly IUserSubscriptionRepository _userSubscriptionRepository;
        private readonly DatabaseFactory.AppContext _dbContext;

        public StripeController(IStripeService stripeService, IUserSubscriptionRepository userSubscriptionRepository, DatabaseFactory.AppContext dbContext)
        {
            _stripeService = stripeService;
            _userSubscriptionRepository = userSubscriptionRepository;
            _dbContext = dbContext;
        }
        [HttpPost]
        public IActionResult CreateSubscription(string parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter))
                {
                    var jsonParam = JsonConvert.DeserializeObject<Dictionary<string, string>>(parameter);
                    string token = jsonParam["token"];
                    string planName = jsonParam["planName"];
                    string planPrice = jsonParam["planPrice"];
                    string userId = jsonParam["userId"];

                    StripeCustomer customer = _stripeService.CreateCustomer(token);

                    if (customer != null)
                    {
                        var planType = new PlanType()
                        {
                            PlanName = "Couchfit premium subscription for " + planName + " " + planPrice,
                            PlanPrice = decimal.Parse(planPrice)
                        };
                        var plan = _stripeService.CreatePlan(planType);

                        if (plan != null)
                        {
                            var subscription = _stripeService.CreateSubscription(customer.Id, plan.Id);

                            if (subscription != null)
                            {
                                planType.PlanName = planName;

                                var subscriptionPlan = _dbContext.subscription_plans.Where(p => p.plan_name == planName).FirstOrDefault();

                                _userSubscriptionRepository.SaveUserSubscription(new UserSubscriptionViewModel
                                {
                                    planType = planType,
                                    userSubscriptionModel = new UserSubscriptionModel
                                    {
                                        subscription_status = "Active",
                                        subscription_type = "Stripe",
                                        subscription_plan_id = subscriptionPlan.id,
                                        date_subscribed = DateTime.UtcNow,
                                        stripe_cus_id = customer.Id,
                                        stripe_plan_id = plan.Id,
                                        stripe_sub_id = subscription.Id,
                                        user_id = userId
                                    }
                                });
                                return Json(new { hasError = false, Message = "Ok" });
                            }
                        }
                    }
                }
                return Json(new { hasError = true, Message = "Invalid Parameter" });
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, ErrorMessage = ex.Message });
                throw;
            }
        }
    }
}