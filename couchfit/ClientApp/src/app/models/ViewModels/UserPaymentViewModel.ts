import { PlanTypeModel } from "../payment/plan-type.model";

export class UserPaymentViewModel {
    StripeToken: string
    StripeCustId: string;
    StripePlanId: string;
    StripeSubId: string;
    PaypalPaymentToken :string
    PaypalAgreementId :string
    UserId :string
    CancelledReason :string
    SuspendedReason:string
    SubscriptionPlansModel : PlanTypeModel
}