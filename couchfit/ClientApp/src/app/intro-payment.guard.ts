import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';

@Injectable({
  providedIn: 'root'
})
export class IntroPaymentGuard implements CanActivate {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.userService.getUserDetails().map(user => {
        if (user.firstName === null && user.lastName === null && user.gender === null) {
          window.location.href = '/onboard/welcome';
        }
        else if (user.skippedPaymentIntro === null) {
          return true;
        } 
        else {
          window.location.href = '/main';
        }
      })
  }
  constructor(private userService: UserService) {

  }
}
