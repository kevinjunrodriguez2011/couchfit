﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.DatabaseModels
{
    public class RoleModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
    }
}
