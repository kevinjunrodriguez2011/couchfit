﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.ViewModels
{
    public class VideoViewModel
    {
        public int VideoId { get; set; }
        public string VideoName { get; set; }
        public string VideoDescription { get; set; }
        public string VideoUrl { get; set; }
        public string VideoThumbnail { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
