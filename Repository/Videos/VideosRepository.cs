﻿using Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Models.ViewModels;
using Interface;
using Microsoft.EntityFrameworkCore;

namespace Repository.Videos
{
    public class VideosRepository: IVideosRepository
    {
        private readonly DatabaseFactory.AppContext _dbContext;

        public VideosRepository(DatabaseFactory.AppContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IReadOnlyCollection<VideoViewModel> getVideos()
        {
            //var videoCategories = _dbContext.videos_categories.ToList();
            var videos = _dbContext.videos.Include(v => v.VideoCategories).ToList();
            var videoViewModelList = new List<VideoViewModel>();
            foreach (var video in videos)
            {
                videoViewModelList.Add(new VideoViewModel
                {
                    VideoId = video.video_id,
                    VideoName = video.video_name,
                    VideoDescription = video.video_description,
                    VideoUrl = video.video_url,
                    VideoThumbnail = video.video_thumbnail,
                    CategoryId = video.VideoCategories?.video_category_id,
                    CategoryName = video.VideoCategories?.category_name
                });
            }
            return videoViewModelList;
        }
        public IReadOnlyCollection<CategoriesVideoModel> getVideosCategories()
        {
            var videoCategories = _dbContext.videos_categories.ToList();
            var videoCategoriesViewModelList = new List<CategoriesVideoModel>();
            foreach (var category in videoCategories)
            {
                videoCategoriesViewModelList.Add(new CategoriesVideoModel
                {
                    CategoryId = category.video_category_id,
                    CategoryName = category.category_name
                });
            }
            return videoCategoriesViewModelList;
        }
        public IReadOnlyCollection<VideoViewModel> getVideosByCategory(int categoryId)
        {
            //var videoCategories = _dbContext.videos_categories.ToList();
            var videos = _dbContext.videos.Where(vid => vid.video_category_id == categoryId).Include(v => v.VideoCategories).ToList();
            var videoViewModelList = new List<VideoViewModel>();

            foreach (var video in videos)
            {
                videoViewModelList.Add(new VideoViewModel
                {
                    VideoId = video.video_id,
                    VideoName = video.video_name,
                    VideoDescription = video.video_description,
                    VideoUrl = video.video_url,
                    VideoThumbnail = video.video_thumbnail,
                    CategoryId = video.VideoCategories?.video_category_id,
                    CategoryName = video.VideoCategories?.category_name
                });
            }
            return videoViewModelList;
        }
        public VideoViewModel getVideosById(int videoId)
        {
            //var videoCategories = _dbContext.videos_categories.ToList();
            var video = _dbContext.videos.Include(v => v.VideoCategories).FirstOrDefault(vid => vid.video_id == videoId);
            var videoViewModelList = new VideoViewModel();

            videoViewModelList.VideoId = video.video_id;
            videoViewModelList.VideoName = video.video_name;
            videoViewModelList.VideoDescription = video.video_description;
            videoViewModelList.VideoThumbnail = video.video_thumbnail;
            videoViewModelList.VideoUrl = video.video_url;
            videoViewModelList.CategoryId = video.video_category_id;
            videoViewModelList.CategoryName = video.VideoCategories.category_name;

            return videoViewModelList;
        }
    }
}
