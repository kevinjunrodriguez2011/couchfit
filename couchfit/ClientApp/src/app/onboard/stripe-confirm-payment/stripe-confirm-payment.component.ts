import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';
import { PlanTypeModel } from '../../models/payment/plan-type.model';
import { PayerModel } from '../../models/payment/payer.model';
import { StripeService } from '../../services/stripe.service';
import{ StripeSubscriptionViewModel } from '../../models/ViewModels/StripeSubscriptionViewModel';
import { UserService } from '../../services/user.service';
import { PaymentService } from '../../services/payment.service';
import { Subscription } from 'rxjs/Subscription';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-stripe-confirm-payment',
  templateUrl: './stripe-confirm-payment.component.html',
  styleUrls: ['./stripe-confirm-payment.component.css'],
  animations: [
    trigger('slidePay', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms', style({transform: 'translateX(100%)'}))
      ])
    ]),

    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class StripeConfirmPaymentComponent implements OnInit {

  stripeSubscriptionViewModel : StripeSubscriptionViewModel;
  selectedPlan : PlanTypeModel;
  redirectStatus : string;
  paymentSubscription : Subscription;
  errorMessage: string;

  constructor(private router: Router, 
    private stripeService: StripeService,
    private userService: UserService,
    private paymentService: PaymentService,
    private spinnerService: NgxSpinnerService) {
    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan')) as PlanTypeModel;
    this.stripeSubscriptionViewModel = this.stripeService.getStripSubscriptionDetails();
    if (this.stripeSubscriptionViewModel.CreditCardNumber == null) {
      this.router.navigate(['onboard/subscribe'])
    }
   }

  ngOnInit() {
  }
  backToPaymentSelection() {
    this.router.navigate(['onboard/subscribe'])
  }
  payWithCC() {
    this.spinnerService.show();
    this.redirectStatus = "Gathering data..."
    this.userService.getUserDetails().subscribe(user => {
      if (user != null) {
        this.redirectStatus = "Processing Payment..."
        this.paymentSubscription = this.paymentService
        .stripeCreateSubscription('tok_visa', this.selectedPlan, user.id)
        .subscribe(res => {
          if (!res.hasError) {
            this.redirectStatus = "Payment Successful!"
            localStorage.setItem('returnSuccess', "yes");
            this.router.navigate(['payment/stripe/return-message'])
            setTimeout(() => {
              this.spinnerService.hide();
            }, 10000);
          }
          else {
            this.errorMessage = res.errorMessage;
            this.spinnerService.hide();
          }
        })
      }
    })
  }
}