﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.DatabaseModels;
using Models.PaymentModels;
using Models.ViewModels;
using Newtonsoft.Json;

namespace couchfit_ng.Controllers
{
    [Authorize(Policy = "User")]
    public class PaypalController : Controller
    {
        private readonly IPaypalService _paypalService;
        private readonly IUserSubscriptionRepository _userSubscriptionRepository;
        public PaypalController(IPaypalService paypalService, IUserSubscriptionRepository userSubscriptionRepository)
        {
            _paypalService = paypalService;
            _userSubscriptionRepository = userSubscriptionRepository;
        }
        [HttpPost]
        public ActionResult CreateBillingAgreement(string parameter)
        {
            try
            {
                Payer payer = JsonConvert.DeserializeObject<Payer>(parameter);

                if (payer != null)
                {
                    var result = _paypalService.CreateBillingAgreement(payer);
                    if (!string.IsNullOrEmpty(result))
                    {
                        var userSubscriptionModel = new UserSubscriptionModel()
                        {
                            subscription_status = "Pending",
                            subscription_type = "Paypal",
                            user_id = payer.UserId
                        };

                        var planType = new PlanType();
                        planType.PlanName = payer.ChosenPlan.PlanName;
                        planType.PlanPrice = payer.ChosenPlan.PlanPrice;

                        _userSubscriptionRepository.SaveUserSubscription(new Models.ViewModels.UserSubscriptionViewModel
                        {
                            userSubscriptionModel = userSubscriptionModel,
                            planType = planType
                        });
                    }
                    return Json(new { paypalBillingLink = result });
                }
                return Json(new { hasError = true, Message = "Invalid payer" });
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true });
            }
        }
        [HttpPost]
        public ActionResult ExecuteBillingAgreement(string parameter)
        {
            try
            {
                var userPaymentViewModel = JsonConvert.DeserializeObject<UserPaymentViewModel>(parameter);
                var result = _paypalService.ExecuteBillingAgreement(userPaymentViewModel);

                if (result != null)
                {
                    var planType = new PlanType();
                    planType.PlanId = userPaymentViewModel.SubscriptionPlansModel.id;
                    planType.PlanName = userPaymentViewModel.SubscriptionPlansModel.plan_name;
                    planType.PlanPrice = userPaymentViewModel.SubscriptionPlansModel.plan_price;

                    var userSubscriptionModel = new UserSubscriptionModel()
                    {
                        date_subscribed = DateTime.UtcNow,
                        subscription_status = "Active",
                        subscription_type = "Paypal",
                        user_id = userPaymentViewModel.UserId,
                        paypal_agreement_id = result.Id
                    };
                    _userSubscriptionRepository.UpdateUserSubscription(new Models.ViewModels.UserSubscriptionViewModel
                    {
                        userSubscriptionModel = userSubscriptionModel,
                        planType = planType
                    });
                    return Json(new { result = result });
                }
                return Json(new { hasError = true });
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true });
                throw;
            }
        }
        [HttpPost]
        public ActionResult CancelBillingAgreement(string parameter)
        {
            try
            {
                var userPaymentViewModel = JsonConvert.DeserializeObject<UserPaymentViewModel>(parameter);
                var result = _paypalService.CancelBillingAgreement(userPaymentViewModel);

                if (result != null)
                {
                    var planType = new PlanType();
                    planType.PlanName = userPaymentViewModel.SubscriptionPlansModel.plan_name;
                    planType.PlanPrice = userPaymentViewModel.SubscriptionPlansModel.plan_price;

                    var userSubscriptionModel = new UserSubscriptionModel()
                    {
                        date_subscribed = DateTime.UtcNow,
                        subscription_status = "Cancelled",
                        subscription_type = "Paypal",
                        user_id = userPaymentViewModel.UserId
                    };
                    _userSubscriptionRepository.UpdateUserSubscription(new Models.ViewModels.UserSubscriptionViewModel
                    {
                        userSubscriptionModel = userSubscriptionModel,
                        planType = planType
                    });
                    return Json(new { result = result });
                }
                return Json(new { hasError = true });
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true });
                throw;
            }
        }
        [HttpPost]
        public ActionResult SuspendBillingAgreement(string parameter)
        {
            try
            {
                var userPaymentViewModel = JsonConvert.DeserializeObject<UserPaymentViewModel>(parameter);
                var result = _paypalService.SuspendBillingAgreement(userPaymentViewModel);

                if (result != null)
                {
                    var planType = new PlanType();
                    planType.PlanName = userPaymentViewModel.SubscriptionPlansModel.plan_name;
                    planType.PlanPrice = userPaymentViewModel.SubscriptionPlansModel.plan_price;

                    var userSubscriptionModel = new UserSubscriptionModel()
                    {
                        date_subscribed = DateTime.UtcNow,
                        subscription_status = "Cancelled",
                        subscription_type = "Paypal",
                        user_id = userPaymentViewModel.UserId
                    };
                    _userSubscriptionRepository.UpdateUserSubscription(new Models.ViewModels.UserSubscriptionViewModel
                    {
                        userSubscriptionModel = userSubscriptionModel,
                        planType = planType
                    });
                    return Json(new { result = result });
                }
                return Json(new { hasError = true });
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true });
                throw;
            }
        }
        [HttpPost]
        public ActionResult ReactivateBillingAgreement(string parameter)
        {
            try
            {
                var userPaymentViewModel = JsonConvert.DeserializeObject<UserPaymentViewModel>(parameter);
                var result = _paypalService.ReactivateBillingAgreement(userPaymentViewModel);

                if (result != null)
                {
                    var planType = new PlanType();
                    planType.PlanName = userPaymentViewModel.SubscriptionPlansModel.plan_name;
                    planType.PlanPrice = userPaymentViewModel.SubscriptionPlansModel.plan_price;

                    var userSubscriptionModel = new UserSubscriptionModel()
                    {
                        date_subscribed = DateTime.UtcNow,
                        subscription_status = "Active",
                        subscription_type = "Paypal",
                        user_id = userPaymentViewModel.UserId
                    };
                    _userSubscriptionRepository.UpdateUserSubscription(new Models.ViewModels.UserSubscriptionViewModel
                    {
                        userSubscriptionModel = userSubscriptionModel,
                        planType = planType
                    });
                    return Json(new { result = result });
                }
                return Json(new { hasError = true });
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true });
                throw;
            }
        }
    }
}