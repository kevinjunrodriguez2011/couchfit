import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideojsComponent } from '../../app/videojs/videojs.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VideojsComponent],
  exports: [VideojsComponent]
})
export class SharedModule { }
