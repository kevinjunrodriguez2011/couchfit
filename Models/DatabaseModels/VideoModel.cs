﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DatabaseModels
{
    public class VideoModel
    {
        [Key]
        public int video_id { get; set; }
        public string video_name { get; set; }
        public string video_description { get; set; }
        public int? instructor_id { get; set; }
        public string instructor_name { get; set; }
        public string video_url { get; set; }
        public string video_thumbnail { get; set; }
        public int video_category_id { get; set; }

        [ForeignKey("video_category_id")]
        public virtual CategoriesModel VideoCategories { get; set; }
    }
}
