import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentRoutingModule } from './payment-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PaypalReturnComponent } from './paypal-return/paypal-return.component';
import { PaypalCancelComponent } from './paypal-cancel/paypal-cancel.component';
import { PaypalReturnMessageComponent } from './paypal-return-message/paypal-return-message.component';
import { StripeReturnMessageComponent } from './stripe-return-message/stripe-return-message.component';

@NgModule({
  imports: [
    CommonModule,
    PaymentRoutingModule,
    NgxSpinnerModule
  ],
  declarations: [PaypalReturnComponent, PaypalCancelComponent, PaypalReturnMessageComponent, StripeReturnMessageComponent]
})
export class PaymentModule { }
