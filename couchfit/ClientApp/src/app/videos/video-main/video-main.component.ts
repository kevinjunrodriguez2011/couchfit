import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit, OnDestroy } from '@angular/core';
import { VideoModel } from '../../models/video-model'
import { VideoEventsService } from '../../services/video-events.service'
import { Observable, Subject } from 'rxjs';
import { VgAPI } from 'videogular2/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-video-main',
  templateUrl: './video-main.component.html',
  styleUrls: ['./video-main.component.css'],
})
export class VideoMainComponent implements OnInit, AfterViewInit, OnDestroy {

  private videoUrl: string;
  videoSources: Array<object>
  videoApi: VgAPI;
  currentVideo: any;
  videoLoaded = false
  videoEventServiceSubscription: any;

  constructor(private videoEventsService: VideoEventsService, 
    private spinner: NgxSpinnerService) {
      this.videoEventServiceSubscription = this.videoEventsService.$videoPlayEvents.subscribe(video => {
        this.videoSources = new Array<Object>();
        this.playNextVideo(video);
        this.videoLoaded = true;
      })
  }

  playNextVideo(video) {
    this.videoSources.push({
      src: video.videoUrl,
      type: "video/mp4"
    })
    //this.ref.detectChanges();
  }

  onPlayerReady(videoApi: VgAPI) {
    this.videoApi = videoApi;
    this.videoApi.getDefaultMedia().subscriptions.loadedData.subscribe(this.playVideo.bind(this));
    this.videoApi.getDefaultMedia().subscriptions.ended.subscribe(this.playNextVideo.bind(this));
  }
  ngOnInit() {

  }
  ngAfterViewInit() {
    
  }
  ngOnDestroy() {
    this.videoEventServiceSubscription.unsubscribe();
  }
  playVideo() {
    try {
      this.videoApi.play(); 
    } catch (error) {
      
    }
  }
  pauseVideo() {
    try {
      this.videoApi.pause(); 
    } catch (error) {
      
    }
  }
}
