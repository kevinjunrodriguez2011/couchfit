import { Component, OnInit } from '@angular/core';
import { SubscriptionPlanService } from '../../../services/subscription-plan.service';
import { PlanTypeModel } from '../../../models/payment/plan-type.model';

@Component({
  selector: 'app-home-bottom',
  templateUrl: './home-bottom.component.html',
  styleUrls: ['./home-bottom.component.css']
})
export class HomeBottomComponent implements OnInit {
  
  planTypeList = new Array<PlanTypeModel>();

  constructor(private subscriptionPlanService: SubscriptionPlanService) { 
    this.subscriptionPlanService.getSubscriptionPlans().subscribe(plans => {
      plans.forEach(element => {
        let planType = new PlanTypeModel();
        planType.PlanName = element.plan_name;
        planType.PlanPrice = element.plan_price;
        this.planTypeList.push(planType);
      });
    })
  }

  ngOnInit() {
  }

}
