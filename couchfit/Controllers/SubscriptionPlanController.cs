﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace couchfit_ng.Controllers
{
    public class SubscriptionPlanController : Controller
    {
        private readonly ISubscriptionPlanRepository _subscriptionPlans;
        public SubscriptionPlanController(ISubscriptionPlanRepository subscriptionPlans)
        {
            _subscriptionPlans = subscriptionPlans;
        }
        public object GetSubscriptionPlans()
        {
            return _subscriptionPlans.GetSubscriptionPlans();
        }
    }
}