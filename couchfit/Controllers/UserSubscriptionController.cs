﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.DatabaseModels;

namespace couchfit_ng.Controllers
{
    [Authorize(Policy = "User")]
    public class UserSubscriptionController : Controller
    {
        private readonly IUserSubscriptionRepository _userSubscriptionRepository;
        public UserSubscriptionController(IUserSubscriptionRepository userSubscriptionRepository)
        {
            _userSubscriptionRepository = userSubscriptionRepository;
        }
        public IActionResult GetUserSubscription(string userId)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    return Json(new { userSubscriptionPlan = _userSubscriptionRepository.GetUserSubscriptionPlan(userId) });
                }
                return Json(new { hasError = true, Message = "No subscription" });
            }
            catch (Exception)
            {
                return Json(new { hasError = true });
                throw;
            }
        }
    }
}