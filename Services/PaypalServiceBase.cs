﻿using BraintreeHttp;
using PayPal.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public abstract class PaypalServiceBase
    {
        private static bool isDev = true;
        public PayPalEnvironment GetCurrentPaypalEnvironment()
        {
            if (isDev)
            {
                return new SandboxEnvironment("AaBe6BvrNomEOkxcXp3nIghmGaVCHcNrILOrF8H9deTpQxzxRafrCdO_zT-aMJebwjP1hhq0af0O0U6o", "EJY0kxRTlP5hbjrFfJqMhUyPxzx3ngsAlCFSDRnfrUE35UkC0oP3eHlbn8vBnDVRwkTlPx_id2vHa8dW");
            }
            return new LiveEnvironment("ARiyKfalYKAnszj3YrCz_DcItCL2rVEnUk6w4MtC-g28d_U4jhLdKJ3zkkgENBxu7y_8Z0QuYB40ck-W", "EC1AllB663A8R44dT_2hje2-o6P_vZ8HtK6aJ3scBeDvnZuuFZOca9_uTGLYInw5F0c6fsLGeaHEXhea");
        }
        public PayPalHttpClient PaypalHttpClient(PayPalEnvironment payPalEnvironment)
        {
            return new PayPalHttpClient(payPalEnvironment);
        }
        public HttpResponse ExecuteRequest(HttpRequest req) 
        {
            try
            {
                var client = new PayPalHttpClient(this.GetCurrentPaypalEnvironment());
                return client.Execute(req).Result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<HttpResponse> ExecuteRequestAsync(HttpRequest req)
        {
            try
            {
                var client = new PayPalHttpClient(this.GetCurrentPaypalEnvironment());
                return await client.Execute(req);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
