﻿using System;
using System.Collections.Generic;
using System.Text;
using Interface;
using Models.PaymentModels;
using Models.PaymentModels.StripeModel;
using Stripe;

namespace Services
{
    public class StripeService : StripeServiceBase, IStripeService
    {
        public StripeCustomer CreateCustomer(string token)
        {
            var options = new StripeCustomerCreateOptions
            {
                Description = "Couchfit new subscriber",
                SourceToken = token
            };

            StripeCustomer customer = this.GetCustomerService().Create(options);
            return customer;
        }
        public StripeCustomer RetrieveCustomer(string cus_id)
        {
            StripeCustomer customer = this.GetCustomerService().Get(cus_id);
            return customer;
        }
        public StripeCustomer UpdateStripeCustomer(string cus_id)
        {
            var options = new StripeCustomerUpdateOptions
            {
                Description = "Couchfit update subscriber",
            };

            StripeCustomer customer = this.GetCustomerService().Update(cus_id, options);
            return customer;
        }
        public StripeDeleted DeleteCustomer(string cus_id)
        {
            StripeDeleted deletedCustomer = this.GetCustomerService().Delete(cus_id);
            return deletedCustomer;
        }
        public StripeList<StripeCustomer> GetAllCustomers()
        {
            var options = new StripeCustomerListOptions
            {
                Limit = 100,
            };
            return this.GetCustomerService().List();
        }
        public StripePlan CreatePlan(PlanType planType)
        {
            string planPrice = "";

            switch (planType.PlanName)
            {
                case "Student":
                    planPrice = PlanPriceConst.Student.ToString();
                    break;
                case "Individual":
                    planPrice = PlanPriceConst.Individual.ToString();
                    break;
                case "Group":
                    planPrice = PlanPriceConst.Group.ToString();
                    break;
            }
            var planOptions = new StripePlanCreateOptions()
            {
                Product = new StripePlanProductCreateOptions()
                {
                    Name = planType.PlanName
                },
                Amount = Convert.ToInt32(planType.PlanPrice * 100),
                Currency = "usd",
                Interval = "month",
                IntervalCount = 1
            };

            StripePlan plan = GetStripePlanService().Create(planOptions);
            return plan;
        }
        public StripePlan RetrievePlan(string planId)
        {
            StripePlan plan = this.GetStripePlanService().Get(planId);
            return plan;

        }
        public StripePlan UpdatePlan(string planId, StripePlanUpdateOptions stripePlanUpdateOptions)
        {
            StripePlan plan = this.GetStripePlanService().Update(planId, stripePlanUpdateOptions);
            return plan;
        }
        public StripeDeleted DeletePlan(string planId)
        {
            StripeDeleted stripeDeleted = this.GetStripePlanService().Delete(planId);
            return stripeDeleted;
        }
        public StripeSubscription CreateSubscription(string cus_id, string plan_id)
        {
            var items = new List<StripeSubscriptionItemOption>
            {
                new StripeSubscriptionItemOption {
                PlanId = plan_id
                }
            };

            var options = new StripeSubscriptionCreateOptions
            {
                CustomerId = cus_id,
                Items = items
            };

            StripeSubscription stripeSubscription = this.GetSubscriptionService().Create(options);
            return stripeSubscription;
        }
        public StripeSubscriptionItem RetrieveSubscriptionItem(string sub_id)
        {
            StripeSubscriptionItem stripeSubscriptionItem = this.GetSubscriptionItemService().Get(sub_id);
            return stripeSubscriptionItem;
        }
        public StripeSubscriptionItem UpdateSubscriptionItem(string sub_id, string plan_id, int quantity)
        {
            var options = new StripeSubscriptionItemUpdateOptions
            {
                PlanId = plan_id,
                Quantity = quantity
            };
            StripeSubscriptionItem stripeSubscriptionItem = this.GetSubscriptionItemService().Update(sub_id, options);
            return stripeSubscriptionItem;
        }
        public StripeDeleted DeleteSubscriptionItem(string sub_id)
        {
            StripeDeleted deletedSubscriptionItem = this.GetSubscriptionItemService().Delete(sub_id);
            return deletedSubscriptionItem;
        }
    }
}
