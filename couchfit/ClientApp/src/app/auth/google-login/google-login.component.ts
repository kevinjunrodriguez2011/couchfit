import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Endpoint } from '../../services/endpoint';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-google-login',
  templateUrl: './google-login.component.html',
  styleUrls: ['./google-login.component.css']
})
export class GoogleLoginComponent implements OnInit {

  private authWindow: Window;
  failed: boolean;
  error: string;
  errorDescription: string;

  constructor(private authService: AuthService, private sharedService: SharedService) { 
    let codeParam = this.sharedService.getParameterByName("code", window.location.href); 
    if (codeParam !== null) { 
      this.googleLogin(codeParam);
    }
  }
  googleLogin(codeParam) {
    if (codeParam !== null) {
      this.authService.googleLogin(codeParam).subscribe(
        result => {
          if (!/Mobi|Android/i.test(navigator.userAgent)) {
            window.close();
          }
          if (!result.hasError) {
            localStorage.setItem('token', result.token);
            if (!/Mobi|Android/i.test(navigator.userAgent)) {
              window.opener.location = '/main';
            }
            else {
              window.location.href = "/main";
            }
          }
          if (result.hasError) {
            alert(result.message);
            window.close();
          }
        },
        error => {
          this.error = error;
        });
    }
  }
  ngOnInit() {
    this.authService.googleLoginEventsSource$.subscribe(() => {
      this.launchGoogleLogin();
    })
  }
  launchGoogleLogin() {
    if (/Mobi|Android/i.test(navigator.userAgent)) {
      window.location.href = 'https://accounts.google.com/o/oauth2/auth?client_id=104151055965-6e02edcc6qnbb97ibgaqul475qshrjtd.apps.googleusercontent.com&redirect_uri='+ Endpoint.currentEndPoint +'/googleauth&scope=openid%20profile%20email%20address&response_type=code&prompt=consent&include_granted_scopes=true',null,'width=600,height=400';
    }
    else {
      window.open('https://accounts.google.com/o/oauth2/auth?client_id=104151055965-6e02edcc6qnbb97ibgaqul475qshrjtd.apps.googleusercontent.com&redirect_uri='+ Endpoint.currentEndPoint +'/googleauth&scope=openid%20profile%20email%20address&response_type=code&prompt=consent&include_granted_scopes=true',null,'width=600,height=400'); 
    }  
  }
}
