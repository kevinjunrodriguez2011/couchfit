import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../app/shared/shared.module';
import { VideosRoutingModule } from './videos-routing.module'
import { HomeComponent } from '../../app/videos/home.component';
import { VideoCarouselComponent } from '../../app/videos/video-carousel/video-carousel.component';
import { VideoMainComponent } from '../../app/videos/video-main/video-main.component';
import { OwlModule } from 'ngx-owl-carousel';
import { NavMenuComponent } from '../../app/videos/nav-menu/nav-menu.component';
import { VideoCarouselCategoryComponent } from '../../app/videos/video-carousel-category/video-carousel-category.component';
import { VideoDetailsComponent } from '../../app/videos/video-details/video-details.component';
import { FooterComponent } from '../../app/videos/footer/footer.component';
import { VideoService } from '../../app/services/video.service'
import { NgxSpinnerModule } from 'ngx-spinner';
import { AllVideosComponent } from './all-videos/all-videos.component';
import { TrimPipe } from '../../app/pipes/trim.pipe';
import { PlayerModule } from '../player/player.module';

@NgModule({
  imports: [
    CommonModule,
    NgxSpinnerModule,
    VideosRoutingModule,
    SharedModule,
    PlayerModule
  ],
  declarations: [
    HomeComponent,
    VideoCarouselComponent,
    VideoMainComponent,
    NavMenuComponent,
    VideoCarouselCategoryComponent,
    VideoDetailsComponent,
    FooterComponent,
    AllVideosComponent,
    TrimPipe,
  ],
  providers: [
    VideoService,
  ],
})
export class VideosModule { }
