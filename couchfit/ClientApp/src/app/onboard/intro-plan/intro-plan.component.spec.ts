import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroPlanComponent } from './intro-plan.component';

describe('IntroPlanComponent', () => {
  let component: IntroPlanComponent;
  let fixture: ComponentFixture<IntroPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
