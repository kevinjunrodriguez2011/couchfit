﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Models.DatabaseModels
{
    public class UserRoleModel : IdentityRole
    {
        public int role_id { get; set; }
        public int user_id { get; set; }

    }
}
