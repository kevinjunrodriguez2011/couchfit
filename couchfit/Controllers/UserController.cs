﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.JWT;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.DatabaseModels;
using Newtonsoft.Json;

namespace couchfit_ng.Controllers
{
    [Authorize(Policy = "User")]
    public class UserController : Controller
    {
        private readonly DatabaseFactory.AppContext _dbContext;
        private readonly UserManager<UserModelIdentity> _userManager;
        private readonly Jwt _jwt;

        public UserController(DatabaseFactory.AppContext dbContext, UserManager<UserModelIdentity> userManager, Jwt jwt)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _jwt = jwt;
        }
        [HttpGet]
        public UserModelIdentity getUserDetails(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token) && token != "null")
                {
                    bool isValidated = _jwt.ValidateToken(token);

                    if (isValidated)
                    {
                        var userClaim = _jwt.GetUserClaim(token);

                        foreach (var claim in userClaim)
                        {
                            if (claim.Type == "User")
                            {
                                var userDetailsClaim = JsonConvert.DeserializeObject<UserModelIdentity>(claim.Value);
                                var userDetails = _dbContext.user.Where(x => x.Id == userDetailsClaim.Id).FirstOrDefault();
                                return userDetails;
                            }
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public async Task<IActionResult> UpdateUserDetails(string parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter))
                {
                    UserDetailsModel userDetailsModel = JsonConvert.DeserializeObject<UserDetailsModel>(parameter);

                    if (userDetailsModel != null)
                    {
                        var user = await _userManager.FindByIdAsync(userDetailsModel.UserId);

                        if (user != null)
                        {
                            user.FirstName = userDetailsModel.FirstName;
                            user.LastName = userDetailsModel.LastName;
                            user.Gender = userDetailsModel.Gender;
                            _dbContext.Update(user);
                            await _dbContext.SaveChangesAsync();

                            return Json(new { Token = _jwt.GenerateToken(user.Email, user), hasError = false, Message = "Ok" });
                        }
                        else
                        {
                            return Json(new { hasError = true, Message = "Invalid User" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true, Message = "System Error" });
            }
            return Json(new { hasError = true, Message = "User details is null" });
        }
        public async Task<IActionResult> SkipIntroPayment(string userId)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    var user = await _userManager.FindByIdAsync(userId);

                    if (user != null)
                    {
                        user.SkippedPaymentIntro = true;
                        _dbContext.Update(user);
                        await _dbContext.SaveChangesAsync();

                        return Json(new { Token = _jwt.GenerateToken(user.Email, user), hasError = false, Message = "Ok" });
                    }
                    else
                    {
                        return Json(new { hasError = true, Message = "Invalid User" });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true, Message = "System Error" });
            }
            return Json(new { hasError = true, Message = "User id is null" });
        }
    }
}