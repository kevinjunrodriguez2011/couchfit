import { Component, EventEmitter, OnInit, ViewChild, ViewChildren, QueryList, Input, AfterViewInit, AfterViewChecked, ElementRef, OnChanges, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { VideoModel } from '../../models/video-model'
import { Observable, Subject } from 'rxjs';
import { VideoService } from '../../services/video.service'
import { VideoEventsService } from '../../services/video-events.service'
import { NgxSpinnerService } from 'ngx-spinner';
import 'rxjs/add/operator/map';
declare var $: any;
import 'slick-carousel';

@Component({
  selector: 'app-video-carousel',
  templateUrl: './video-carousel.component.html',
  styleUrls: ['./video-carousel.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class VideoCarouselComponent implements OnInit, AfterViewInit {

  @ViewChild('carouselPlayIcon') carouselPlayIcon: any;
  @Input() showCarousel;
  videoList$: Observable<VideoModel[]>;
  images: Array<string>;
  carouselPlayIconShow: boolean = false;
  videoList = [];
  videoListCopy = [];
  carouselLoaded = false;

  constructor(private videoService: VideoService,
    private videoEventsService: VideoEventsService,
    private spinner: NgxSpinnerService,
    private ref: ChangeDetectorRef) {
    this.videoList$ = this.videoService.getVideos();
    this.videoList$.subscribe(res => {
      this.videoEventsService.onVideoPlay(res[0])
      this.videoService.videoList = res;
      this.videoList = res;
      this.videoListCopy = res;
      this.initCarousel(false, 5);
      this.carouselLoaded = true;
    })
  }
  ngOnInit() {
    this.videoEventsService.$videoSelectCategoryEvents.subscribe(categoryId => {
      let cloned = this.videoListCopy.map(x => Object.assign({}, x));
      this.videoList = cloned.filter(data => {
        if (data.categoryId === categoryId) {
          return data;
        }
      })
      this.initCarousel(true, this.videoList.length);
    })
  }
  initCarousel(reinit, videoLength) {
    var $owl = $('.video-carousel');
    if (reinit){
      $owl.owlCarousel('destroy'); 
      $owl.find('.owl-stage-outer').children().unwrap();
      $owl.removeClass("owl-center owl-loaded");
    }
    setTimeout(() => { 
      $owl.children().each(function (index) {
        $(this).attr('data-position', index);
      });
      $owl.owlCarousel({
        items: videoLength,
        loop: true,
        video:true,
        margin: 10,
        center: true,
        dots: false,
        nav: true,
        navText: ["<i class='fa fa-angle-double-left'></i>", "<i class='fa fa-angle-double-right'></i>"],
        responsive: {
          0: {
            items: 1,
          },
          600: {
            items: 3
          },
          1000: {
            items: videoLength,
          }
        }
      });

      $('#customNextBtn').click(function () {
        $owl.trigger('next.owl.carousel', [300]);
      })
      // Go to the previous item
      $('#customPrevBtn').click(function () {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        $owl.trigger('prev.owl.carousel', [300]);
      })
      $('.title-container').css('display', 'block')
      $owl.on('click', '.owl-item > div', function () {
        var owlItem = $(this);
        $owl.trigger('to.owl.carousel', owlItem.data('position'));
      });
      let self = this;
      $('.video-carousel-container').on('click', '.play-carousel' ,function () {
        let video = $(this).find('.video-play-details');
        self.selectVideo(JSON.parse(video.html()))
      });
      $('.video-carousel-container').on('click', '.playArrowDownContainer' ,function () {
        let video = $(this).find('.video-details');
        self.selectVideoDetails(JSON.parse(video.html()))
      });
      $('.play-carousel').css('display', 'none')
      $('.playArrowDownContainer').css('display', 'none')
      $owl.on('mouseover', '.owl-item > div', function () {
        var owlItem = $(this);
        $('.play-carousel').css('display', 'none')
        $('.playArrowDownContainer').css('display', 'none')
        owlItem.find('.play-carousel').removeAttr('hidden')
        owlItem.find('.play-carousel').css('display', 'block')
        owlItem.find('.playArrowDownContainer').removeAttr('hidden')
        owlItem.find('.playArrowDownContainer').css('display', 'block')
      })
    }, 10)
  }

  selectVideo(video: VideoModel) {
    this.videoEventsService.onVideoPlay(video)
  }
  selectVideoDetails(video: VideoModel) {
    this.videoEventsService.onVideoSelectDetails(video);
  }
  ngAfterViewInit() {

  }
  ngAfterViewChecked() {

  }
}
