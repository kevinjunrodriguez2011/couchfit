﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.PaymentModels
{
    public class PlanPriceConst
    {
        public const double Student = 4.99;
        public const double Individual = 9.99;
        public const double Group = 14.99;

    }
}
