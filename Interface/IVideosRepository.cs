﻿using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface
{
    public interface IVideosRepository
    {
        IReadOnlyCollection<VideoViewModel> getVideos();
        IReadOnlyCollection<CategoriesVideoModel> getVideosCategories();
        IReadOnlyCollection<VideoViewModel> getVideosByCategory(int categoryId);
        VideoViewModel getVideosById(int videoId);
    }
}
