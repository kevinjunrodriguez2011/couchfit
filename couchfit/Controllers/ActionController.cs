﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Repository.Videos;

namespace couchfit_ng.Controllers
{
    public class ActionController : Controller
    {
        [Route("api/action/test")]
        public IActionResult Test()
        {
            return Json(new { Result = "Ok" });
        }
        [Route("api/action/do")]
        public IActionResult Do()
        {
            try
            {
                var authHeader = this.HttpContext.Request.Headers["auth"];
                var methodName = this.HttpContext.Request.Headers["action"];
                var assemblyName = this.HttpContext.Request.Headers["root"];
                var className = this.HttpContext.Request.Headers["class"];
                var parameters = this.HttpContext.Request.Headers["parameters"];

                if (string.IsNullOrEmpty(authHeader.ToString())
                    || string.IsNullOrEmpty(methodName.ToString())
                    || string.IsNullOrEmpty(assemblyName.ToString()))
                {
                    return Json(new
                    {
                        Result = "Response failed.",
                    });
                }
                try
                {
                    Assembly assembly = Assembly.Load(assemblyName);
                    var nameSpace = assembly.DefinedTypes.Where(x => x.AssemblyQualifiedName.ToLower().Contains(className.ToString().ToLower())).FirstOrDefault();
                    var parameterList = new object[1];

                    if (nameSpace != null)
                    {
                        Type type = assembly.GetType(nameSpace.FullName);
                        if (type != null)
                        {
                            MethodInfo method = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
                            Type paramType = null;
                            if (method != null)
                            {
                                if (!string.IsNullOrEmpty(parameters))
                                {
                                    var jsonParam = JObject.Parse(parameters).ToObject<Dictionary<string, object>>();
                                    foreach (ParameterInfo parameter in method.GetParameters())
                                    {
                                        foreach (var param in jsonParam)
                                        {
                                            if (jsonParam[parameter.Name] != null)
                                            {
                                                paramType = jsonParam[parameter.Name].GetType();
                                                if (paramType.Name == "Int64")
                                                {
                                                    parameterList[0] = (int)(long)jsonParam[parameter.Name];
                                                }
                                                else if (paramType.Name == "Int32")
                                                {
                                                    parameterList[0] = (int)jsonParam[parameter.Name];
                                                }
                                                else
                                                {
                                                    parameterList[0] = jsonParam[parameter.Name];
                                                }
                                                break;
                                            }
                                        }
                                    }

                                }

                                object classInstance = Activator.CreateInstance(type, null);
                                object result = null;
                                result = method.Invoke(classInstance, string.IsNullOrEmpty(parameters) ? new object[] { } : parameterList);
                                if (result != null)
                                {
                                    var returnValue = new
                                    {
                                        Result = result,
                                    };
                                    return Json(returnValue);
                                }
                            }
                            else
                            {
                                return Json(new { Result = "No such action" });
                            }
                        }
                    }
                    else
                    {
                        return Json(new { Result = "No such name" });
                    }
                }
                catch (System.IO.FileNotFoundException)
                {
                    return Json(new { Result = "No such root" });
                }
                return Json(new { Result = "No data" });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}