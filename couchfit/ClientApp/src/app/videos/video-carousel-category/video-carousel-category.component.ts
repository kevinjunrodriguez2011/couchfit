import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { VideoService } from '../../services/video.service'
import { VideoEventsService } from '../../services/video-events.service'
import { Observable, Subject } from 'rxjs';
import { VideosCategoryModel } from '../../models/videoscategory-model'

@Component({
  selector: 'app-video-carousel-category',
  templateUrl: './video-carousel-category.component.html',
  styleUrls: ['./video-carousel-category.component.css']
})

export class VideoCarouselCategoryComponent implements OnInit, AfterViewInit, OnDestroy {

  videosCategory$ = new Observable<VideosCategoryModel[]>();
  videoCategorySubscription: any;
  videoCategories: any;
  categoriesLoaded = false;

  constructor(private videoService: VideoService, private videoEventService: VideoEventsService) {
    this.videosCategory$ = this.videoService.getVideosCategories();
  }

  ngOnInit() {
    this.videoCategorySubscription = this.videosCategory$.subscribe(vidCat => {
      this.videoCategories = vidCat;
      this.categoriesLoaded = true;
    });
  }
  ngAfterViewInit() {

  }
  selectCategory(videoCategoryId: number) {
    this.videoEventService.onVideoSelectCategory(videoCategoryId);
  }
  openAllVideoModal() {
    this.videoEventService.onVideoSelectAll();
  }
  ngOnDestroy() {
    this.videoCategorySubscription.unsubscribe();
  }
}
