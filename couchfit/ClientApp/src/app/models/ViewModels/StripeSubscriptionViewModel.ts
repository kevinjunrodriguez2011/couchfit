export class StripeSubscriptionViewModel {
    StripeToken: string;
    FirstName: string;
    CreditCardNumber: string;
    CreditExpiry: string;
    CVC: string;
}