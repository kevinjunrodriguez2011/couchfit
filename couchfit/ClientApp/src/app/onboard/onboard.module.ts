import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IntroComponent } from './intro/intro.component';
import { IntroPaymentComponent } from './intro-payment/intro-payment.component';
import { OnboardRoutingModule } from './onboard-routing.module';
import { IntroPlanComponent } from './intro-plan/intro-plan.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PaypalConfirmPaymentComponent } from './paypal-confirm-payment/paypal-confirm-payment.component';
import { StripeConfirmPaymentComponent } from './stripe-confirm-payment/stripe-confirm-payment.component';
import { StripeService } from '../services/stripe.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OnboardRoutingModule,
    NgxSpinnerModule
  ],
  providers: [StripeService],
  declarations: [IntroComponent, IntroPaymentComponent, IntroPlanComponent, PaypalConfirmPaymentComponent, StripeConfirmPaymentComponent]
})
export class OnboardModule { }
