import { Component, OnInit } from '@angular/core';
import { StartupService } from '../../../startup/startup.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  constructor(public startUpService: StartupService, private router: Router) { 

  }

  ngOnInit() {

  }
  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('confirmemail');
    this.router.navigate(['/main'])
  }
}
