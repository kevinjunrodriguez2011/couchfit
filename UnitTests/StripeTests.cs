﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models.PaymentModels;
using Models.PaymentModels.StripeModel;
using Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTests
{
    [TestClass]
    public class StripeTests
    {
        StripeService stripeService;
        public StripeTests()
        {
            stripeService = new StripeService();
        }
        [TestMethod]
        public void StripeCreateCustomer()
        {
            var customer = stripeService.CreateCustomer("tok_1DOOqh2eZvKYlo2C6UVofoSF");
            Assert.IsNotNull(customer);
        }
        [TestMethod]
        public void StripeCreatePlan()
        {
            var planModel = new PlanType()
            {
                PlanName = "Couchfit premium subscription for individual $9.99",
                PlanPrice = 9.99m
            };
            var plan = stripeService.CreatePlan(planModel);
            Assert.IsNotNull(plan);
        }
        [TestMethod]
        public void StripeCreateSubscription()
        {
            var customer = stripeService.CreateCustomer("tok_visa");
            var planModel = new PlanType()
            {
                PlanName = "Couchfit premium subscription for individual $9.99",
                PlanPrice = 9.99m
            };
            var plan = stripeService.CreatePlan(planModel);
            var subscription = stripeService.CreateSubscription(customer.Id, plan.Id);
            Assert.IsNotNull(subscription);
        }
    }
}
