import { Component, OnInit, OnDestroy, ElementRef, Input, AfterContentInit, AfterViewInit } from '@angular/core';
import { VideoEventsService } from '../../services/video-events.service';
import { ISubscription } from 'rxjs/Subscription';
import { Router } from '@angular/router'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit, AfterContentInit, OnDestroy {
  
  videoSources = new Array<object>();
  currentVideo: any;
  showVideoControls = false;
  videoEventsServiceSubscription: any;
  videoPause: any;

  constructor(private videoEventsService: VideoEventsService, 
    private router: Router) {
      this.currentVideo = this.videoEventsService.currentPlayerVideo;
      if (this.videoEventsService.currentPlayerVideo == undefined) {
        this.back();
      }
   }

  ngOnInit() {
    this.videoEventsService.$videoPlayAllEvents.subscribe(video => {
      this.videoSources = new Array<object>();
      this.videoSources.push({
        src: video.videoUrl,
        type: "video/mp4"
      });
    })
    if (this.currentVideo !== undefined) {
      this.videoSources.push({
        src: this.currentVideo.videoUrl,
        type: "video/mp4"
      })
    }
  }
  ngAfterContentInit() {

  }
  ngOnDestroy() {
    if (this.videoEventsService.allVideosEventSubscription !== undefined) {
      this.videoEventsService.allVideosEventSubscription.unsubscribe();
    }
  }
  videoPlaying(event) {
    if (event) {
      this.showVideoControls = false;
    }
  }
  videoPaused(event) {
    if (event) {
      this.videoPause = true;
      this.showVideoControls = true;
    }
  }
  back() {
    this.router.navigate(['/videos']);
  }
  showAllVideosModal() {
    this.videoEventsService.onShowAllVideosModal();
  }
  showVideoMenu() {
    this.showVideoControls = true;
    // if (!this.videoPause) {
    //   setTimeout(() => {
    //     this.showVideoControls = false;
    //   }, 4000);
    // }
  }
}