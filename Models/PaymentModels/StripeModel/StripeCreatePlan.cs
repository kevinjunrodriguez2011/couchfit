﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.PaymentModels.StripeModel
{
    public class StripeCreatePlan
    {
        public double PlanPrice { get; set; }
        public string PlanDescription { get; set; }
    }
}
