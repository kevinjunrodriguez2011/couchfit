import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { UserSubscriptionService } from '../services/user-subscription.service';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  public isAuthenticated = false;
  currentUser: any;
  
  constructor(private httpClient: HttpClient, private userService: UserService, private userSubscriptionService: UserSubscriptionService) { }

  load() {
    if (!window.location.pathname.match(/(onboard|payment)/) ) {
      let token = localStorage.getItem('token');
      if (token != null) {
        this.userService.getUserDetails().subscribe(user => {   
          if (user == null) {
            localStorage.removeItem('token');
            localStorage.removeItem('confirmemail');
            window.location.href = '/main';
          }
          else {
            this.userSubscriptionService.getUserSubscription(user.id).subscribe(userSub => {
              if (userSub == null || userSub.hasError || userSub.userSubscriptionPlan.subscriptionStatus === 'Pending') {
                if (user.firstName === null && user.lastName === null && user.gender === null) {
                  window.location.href = '/onboard/welcome';
                } 
                if (user.skippedPaymentIntro === null) {
                  window.location.href = '/onboard/plans';
                }
              }
            })
          }
        })
      }
    }
    this.authenticate().subscribe(auth => {
      this.isAuthenticated = auth
    });
  }
  authenticate() {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.httpClient.get<any>('api/auth/validatetoken?token=' + token, httpOptions)
  }
}
