﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.DatabaseModels
{
    public class SubscriptionPlansModel
    {
        [Key]
        public int id { get; set; }
        public string plan_name { get; set; }
        public decimal plan_price { get; set; }
    }
}
