import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-paypal-return-message',
  templateUrl: './paypal-return-message.component.html',
  styleUrls: ['./paypal-return-message.component.css'],
  animations: [
    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class PaypalReturnMessageComponent implements OnInit {

  constructor(private router: Router) { 
    var isReturnSuccess = localStorage.getItem('returnSuccess')
    if (isReturnSuccess == undefined) {
      window.location.href = "/main";
    }
  }

  ngOnInit() {
  }
  browseVideos() {
    this.router.navigate(['/videos']);
    localStorage.removeItem('returnSuccess');
  }
}
