import { Component, OnInit } from '@angular/core';
import { RegisterModel } from '../../models/register-model';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerModel: RegisterModel;
  errors = [];
  hasError = false;
  registering = false;

  constructor(private authService: AuthService, private router: Router) { 
    this.registerModel = new RegisterModel();
  }

  ngOnInit() {
  }

  register() {
    this.registering = true;
    let isValid = true;
    if (this.registerModel.Email != undefined && this.registerModel.ConfirmEmail != undefined) {
      if (this.registerModel.Email !== this.registerModel.ConfirmEmail) {
        this.hasError = true;
        isValid = false;
        this.errors.push({'description': 'Email and confirm email does not match'});
      }
      if (this.registerModel.Password !== undefined && this.registerModel.Password.length < 5) {
        this.hasError = true;
        isValid = false;
        this.errors.push({'description': 'Your password must be atleast 6 characters long'});
      }
      if (this.registerModel.Password === undefined || this.registerModel.Password === '') {
        this.hasError = true;
        isValid = false;
        this.errors.push({'description': 'Password is empty'});
      }
      if (this.registerModel.Password !== this.registerModel.ConfirmPassword) {
        this.hasError = true;
        isValid = false;
        this.errors.push({'description': 'Password and confirm password are not equal'});
      }
      if (!this.validateEmail(this.registerModel.Email)) {
        this.hasError = true;
        isValid = false;
        this.errors.push({'description': 'Invalid email address'});
      }
      if (isValid) {
        this.authService.register(this.registerModel).subscribe(res => {
          if (!res.hasError) {
            localStorage.setItem('confirmemail', 'true');
            this.router.navigate(['/confirmemail'])
          }
          else if (res.hasError) {
            this.errors = [];
            this.hasError = true;
            if (res.emailTaken) {
              this.errors.push({'description': 'Email already registered.'});
            }
            if (res.SystemError) {
              this.errors.push({'description': 'Register is unavailable right now, please try again later'});
            }
            this.registerModel.ConfirmEmail = '';
            this.registerModel.Password = '';
          }
        })
      }
    }
    else {
      this.errors = [];
      this.hasError = true;
      this.errors.push({'description': 'Required fields are empty'});
    }
    setTimeout(() => {
      this.errors = [];
    }, 4000)
  }
  openFbLogin() {
    this.authService.onFacebookLogin();
  }
  openGoogleLogin() {
    this.authService.onGoogleLogin();
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
