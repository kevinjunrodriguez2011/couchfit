import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StripeConfirmPaymentComponent } from './stripe-confirm-payment.component';

describe('StripeConfirmPaymentComponent', () => {
  let component: StripeConfirmPaymentComponent;
  let fixture: ComponentFixture<StripeConfirmPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StripeConfirmPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StripeConfirmPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
