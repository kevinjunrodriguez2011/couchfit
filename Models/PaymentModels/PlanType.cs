﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.PaymentModels
{
    public class PlanType
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public decimal PlanPrice { get; set; }
    }
}
