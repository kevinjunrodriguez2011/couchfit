﻿using Interface;
using Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Repository
{
    public class SubscriptionPlanRepository : ISubscriptionPlanRepository
    {
        private readonly DatabaseFactory.AppContext _dbContext;
        public SubscriptionPlanRepository(DatabaseFactory.AppContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IReadOnlyCollection<SubscriptionPlansModel> GetSubscriptionPlans()
        {
            IReadOnlyCollection<SubscriptionPlansModel> subscriptionPlans = _dbContext.subscription_plans.ToList();
            return subscriptionPlans;
        }
    }
}
