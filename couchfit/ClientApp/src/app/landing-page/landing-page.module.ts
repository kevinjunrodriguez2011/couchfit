import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageRoutingModule } from './landing-page-routing.module';
import { HomeComponent } from './home/home.component';
import { HomeBottomComponent } from './home/home-bottom/home-bottom.component';
import { HomeTopComponent } from './home/home-top/home-top.component';
import { HomeMiddleComponent } from './home/home-middle/home-middle.component';
import { NavMenuComponent } from './home/nav-menu/nav-menu.component';
import { FooterComponent } from './home/footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    LandingPageRoutingModule
  ],
  declarations: [
    HomeComponent, 
    HomeBottomComponent, 
    HomeTopComponent, 
    HomeMiddleComponent, 
    NavMenuComponent, 
    FooterComponent
  ]
})
export class LandingPageModule { }
