﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models.DatabaseModels
{
    public class CategoriesModel
    {
        [Key]
        public int video_category_id { get; set; }
        public string category_name { get; set; }
        public DateTime date_created { get; set; }
        public virtual ICollection<VideoModel> Videos { get; set; }
    }
}
