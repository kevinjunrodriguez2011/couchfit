import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoCarouselCategoryComponent } from './video-carousel-category.component';

describe('VideoCarouselCategoryComponent', () => {
  let component: VideoCarouselCategoryComponent;
  let fixture: ComponentFixture<VideoCarouselCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoCarouselCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoCarouselCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
