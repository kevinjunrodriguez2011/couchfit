import { Component, OnInit, ViewChild, TemplateRef, OnDestroy, ViewEncapsulation } from '@angular/core';
import { VideoModel } from '../../models/video-model'
import { Observable, Subject } from 'rxjs';
import { VideoService } from '../../services/video.service'
import { ModalService } from '../../services/modal.service'
import { VideoEventsService } from '../../services/video-events.service'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router'
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import {
  trigger,
  state,
  style,
  animate,
  group,
  transition
} from '@angular/animations';
declare var $: any;
export const SlideInOutAnimation = [
  trigger('videoDetailsState', [
    state('selected', style({
      'max-height': '500px', 'opacity': '1', 'visibility': 'visible'
    })),
    state('unselected', style({
      'max-height': '0px', 'opacity': '0', 'visibility': 'hidden'
    })),
    transition('selected => unselected', [group([
      animate('400ms ease-in-out', style({
        'opacity': '0'
      })),
      animate('600ms ease-in-out', style({
        'max-height': '0px'
      })),
      animate('700ms ease-in-out', style({
        'visibility': 'hidden'
      }))
    ]
    )]),
    transition('unselected => selected', [group([
      animate('1ms ease-in-out', style({
        'visibility': 'visible'
      })),
      animate('600ms ease-in-out', style({
        'max-height': '500px'
      })),
      animate('800ms ease-in-out', style({
        'opacity': '1'
      }))
    ]
    )]),
  ]),
]
@Component({
  selector: 'app-all-videos',
  templateUrl: './all-videos.component.html',
  styleUrls: ['./all-videos.component.css'],
  animations: [SlideInOutAnimation],
  encapsulation: ViewEncapsulation.None,
})

export class AllVideosComponent implements OnInit, OnDestroy {

  videoList$: Observable<VideoModel[]>;
  videoList = new Array<VideoModel>();
  videoSearchTerm = new Subject<string>();
  modalRef: any;
  videoGroupedList = [];
  videoGroupedListBackup = [];
  @ViewChild('allVideoTemplate') allVideoTemplate: TemplateRef<any>
  videoDetails = new VideoModel();
  videoDetailsShow = false;
  currentSelectedVideo: VideoModel;
  videoEventServiceSubscription: any;
  videoAllEventServiceSubscription: any;
  public videoSelectState = ''

  constructor(private videoService: VideoService,
    private modalService: ModalService,
    private videoEventsService: VideoEventsService,
    private router: Router) {
    this.videoList$ = this.videoService.getVideos();

  }
  loadAllVideosCarousel(reinit) {
    setTimeout(() => {
      for (let i = 0; i < this.videoGroupedList.length; i++) {
        var $owl = $('.' + i + this.videoGroupedList[i].key.replace(/\s/g, ""));
        if (reinit) {
          $owl.owlCarousel('destroy');
          $owl.find('.owl-stage-outer').children().unwrap();
          $owl.removeClass("owl-center owl-loaded");
        }
        $owl.owlCarousel({
          items: 5,
          loop: true,
          margin: 10,
          dots: false,
          center: true,
          nav: true,
          navText: ["<i class='fa fa-angle-double-left'></i>", "<i class='fa fa-angle-double-right'></i>"],
          responsive: {
            0: {
              items: 1,
            },
            600: {
              items: 3
            },
            1000: {
              items: 5,
            }
          }
        });
        $('#customNextBtn').click(function () {
          $owl.trigger('next.owl.carousel', [300]);
        })
        $('#customPrevBtn').click(function () {
          $owl.trigger('prev.owl.carousel', [300]);
        })
        $('.title-container').css('display', 'block')
        let self = this;
        $('.all-video-carousel-container').on('click', '.all-video-play-carousel', function () {
          let video = JSON.parse($(this).find('.all-video-play-details').html()) as VideoModel;
          self.playVideo(video)
        });
        $('.all-video-carousel-container').on('click', '.all-video-playArrowDownContainer', function () {
          let video = JSON.parse($(this).find('.all-video-details').html()) as VideoModel;
          $(".videoDetails").addClass('easeIn');
          self.getVideoDetails(video.videoId);
          self.currentSelectedVideo = video;
          self.toggleVideoSelectState();
          setTimeout(() => {
            $(".videoDetails").removeClass('easeIn');
          }, 500)
        });
        $('.all-video-play-carousel').css('display', 'none')
        $('.all-video-playArrowDownContainer').css('display', 'none')
        $owl.on('mouseover', '.owl-item > div', function () {
          var owlItem = $(this);
          $('.all-video-play-carousel').css('display', 'none')
          $('.all-video-playArrowDownContainer').css('display', 'none')
          owlItem.find('.all-video-play-carousel').removeAttr('hidden')
          owlItem.find('.all-video-play-carousel').css('display', 'block')
          owlItem.find('.all-video-playArrowDownContainer').removeAttr('hidden')
          owlItem.find('.all-video-playArrowDownContainer').css('display', 'block')
        })
      }
    })
  }
  selectVideo(owlClass) {
    let owlCarousel = $('.' + owlClass.replace(/\s/g, ""));
    owlCarousel.on('click', '.owl-item > div', function () {
      var owlItem = $(this);
      owlCarousel.trigger('to.owl.carousel', owlItem.data('position'));
    });
  }
  toggleVideoSelectState() {
    this.videoSelectState = this.videoSelectState == 'unselected' ? 'selected' : 'unselected';
  }
  toggleVideoUnselectedState() {
    this.videoSelectState = this.videoSelectState == 'unselected' ? 'selected' : 'unselected';
  }
  playVideo(video) {
    this.videoEventsService.onVideoPlayAll(video);
    this.videoEventsService.currentPlayerVideo = video;
    this.modalRef.hide();
    this.router.navigate(['/player']);
  }
  playButtonVideo() {
    this.videoEventsService.currentPlayerVideo = this.currentSelectedVideo;
    this.videoEventsService.onVideoPlayAll(this.currentSelectedVideo);
    this.modalRef.hide();
    this.router.navigate(['/player']);
  }
  getVideoDetails(videoId) {
    this.videoDetails = this.videoList.filter(vid => vid.videoId == videoId)[0];
  }
  ngOnInit() {
    this.videoEventServiceSubscription = this.videoEventsService.$videoSelectAllEvents.subscribe(() => {
      this.videoList = this.videoService.videoList;
      const groupedObj = this.videoList.reduce((prev, cur) => {
        if (!prev[cur['categoryName']]) {
          prev[cur['categoryName']] = [cur];
        } else {
          prev[cur['categoryName']].push(cur);
        }
        return prev;
      }, {});
      this.videoGroupedList = Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
      this.videoGroupedListBackup = Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
      this.loadAllVideosCarousel(false);
      this.openModal();
    })
    this.videoEventsService.allVideosEventSubscription = this.videoEventsService.$allVideosModalEvents.subscribe(() => {
      this.openModal();
      this.loadAllVideosCarousel(false);
    })
  }
  searchVideo(searchTerm: string) {
    setTimeout(() => {
      if (searchTerm.trim() !== '') {
        let filterVideoGroupedList = this.videoGroupedListBackup.filter(d => d.key.toLowerCase().trim().indexOf(searchTerm.toLowerCase().trim()) !== -1);
        let hasFilter = filterVideoGroupedList.length > 0;
        if (hasFilter) {
          this.videoGroupedList = filterVideoGroupedList;
          this.loadAllVideosCarousel(true);
        }
      }
      else {
        this.videoGroupedList = this.videoGroupedListBackup;
        this.loadAllVideosCarousel(true);
      }
    }, 200)
  }
  openModal() {
    this.modalRef = this.modalService.openModal(this.allVideoTemplate);
  }
  ngOnDestroy() {
    this.videoEventServiceSubscription.unsubscribe();
  }
}
