﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.JWT;
using Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Models.DatabaseModels;
using Models.FacebookAuthModels;
using Models.GoogleAuthModels;
using Newtonsoft.Json;

namespace couchfit_ng.Controllers
{
    public class AuthController : Controller
    {
        private readonly DatabaseFactory.AppContext _dbContext;
        private readonly UserManager<UserModelIdentity> _userManager;
        private readonly Jwt _jwt;
        private readonly IEmailHelper _emailHelper;
        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IActionContextAccessor _actionContextAccessor;
        private readonly string environment = "http://couchfit.ca";

        public AuthController(DatabaseFactory.AppContext dbContext, UserManager<UserModelIdentity> userManager, Jwt jwt, IEmailHelper emailHelper, IUrlHelperFactory urlHelperFactory, IActionContextAccessor actionContextAccessor)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _jwt = jwt;
            _emailHelper = emailHelper;
            _urlHelperFactory = urlHelperFactory;
            _actionContextAccessor = actionContextAccessor;
        }
        [HttpPost]
        public async Task<IActionResult> Register(string email, string password)
        {
            try
            {
                var user = _userManager.FindByEmailAsync(email).Result;
                if (user != null)
                {
                    return Json(new { hasError = true, EmailTaken = true });
                }
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
                {
                    UserModelIdentity userModelIdentity = new UserModelIdentity
                    {
                        Email = email,
                        UserName = email,
                        DateCreated = DateTime.UtcNow,
                        Provider = "Direct",
                        EmailConfirmed = false,
                    };
                    var hashedPassword = _userManager.PasswordHasher.HashPassword(userModelIdentity, password);
                    userModelIdentity.PasswordHash = hashedPassword;

                    Task<IdentityResult> createUser = _userManager.CreateAsync(userModelIdentity);

                    if (createUser.Result.Succeeded)
                    {
                        await _emailHelper.SendEmailAsync(userModelIdentity.Email, "Hello user, <br/><br/> Thanks for signing up to couchfit, confirm your email to access our free videos. Click <a href=" + _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext).Action("ConfirmEmail", "Auth", new { userId = userModelIdentity.Id }, Request.Scheme) + ">Here</a>", "Confirm Email");
                        return Json(new { hasError = false, Message = "Ok" });
                    }
                    else
                    {
                        return Json(new { hasError = true, Message = createUser.Result.Errors });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true, Message = "Register service is unavailable right now, please try again later" });
            }
            return Json(new { SystemError = true, hasError = true, Message = "Error" });
        }
        [HttpPost]
        public async Task<IActionResult> Login(string email, string password)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (user != null)
                {
                    PasswordVerificationResult passwordVerificationResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
                    if (passwordVerificationResult == PasswordVerificationResult.Success)
                    {
                        if (user.Provider != "Direct")
                        {
                            return Json(new { hasError = true, Direct = false });
                        }
                        if (!user.EmailConfirmed)
                        {
                            return Json(new { hasError = true, EmailUnconfirmed = true });
                        }
                        if (passwordVerificationResult == PasswordVerificationResult.Failed)
                        {
                            return Json(new { hasError = true, Message = "Email or password is incorrect" });
                        }
                        if (passwordVerificationResult == PasswordVerificationResult.SuccessRehashNeeded)
                        {
                            //implement
                        }
                        return Json(new { hasError = false, Token = _jwt.GenerateToken(email, user) });
                    }
                    else
                    {
                        return Json(new { hasError = true, Message = "Email or password is incorrect" });
                    }
                }
                else
                {
                    return Json(new { hasError = true, Message = "Email or password is incorrect" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { SystemError = true, hasError = true, Message = "Login service is unavailable right now, please try again later" });
            }
        }
        [HttpPost]
        public async Task<IActionResult> ConfirmEmail(string userId)
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    var user = _userManager.FindByIdAsync(userId).Result;
                    if (user != null && !user.EmailConfirmed)
                    {
                        user.EmailConfirmed = true;
                        _dbContext.Update(user);
                        await _dbContext.SaveChangesAsync();
                        return Json(new { hasError = false, Message = "Confirmed", Token = _jwt.GenerateToken(user.Email, user) });
                    }
                    return Json(new { hasError = false, Message = "Already Confirmed" });
                }
                return Json(new { hasError = true, Message = "Error" });
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, Message = "System Error" });
            }
        }
        public async Task<IActionResult> FacebookLogin(string accessToken)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var appAccessTokenResponse = await httpClient.GetStringAsync($"https://graph.facebook.com/v3.1/oauth/access_token?client_id=2228201457416269&client_secret=78f0006e96f4af31e78f2493137c3e87&grant_type=client_credentials");
                    var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);

                    var userAccessTokenValidationResponse = await httpClient.GetStringAsync($"https://graph.facebook.com/v3.1/debug_token?input_token={accessToken}&access_token={appAccessToken.AccessToken}");
                    var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation>(userAccessTokenValidationResponse);

                    if (!userAccessTokenValidation.Data.IsValid)
                    {
                        return Json(new { hasError = true, Message = "Invalid facebook token." });
                    }
                    var userInfoResponse = await httpClient.GetStringAsync($"https://graph.facebook.com/v3.1/me?fields=email,first_name,middle_name,last_name,locale,birthday,picture&access_token={accessToken}");

                    var userInfo = JsonConvert.DeserializeObject<FacebookUserData>(userInfoResponse);

                    var user = await _userManager.FindByEmailAsync(userInfo.Email);

                    if (user == null)
                    {
                        var appUser = new UserModelIdentity
                        {
                            FirstName = userInfo.FirstName,
                            LastName = userInfo.LastName,
                            MiddleName = userInfo.MiddleName,
                            Email = userInfo.Email,
                            UserName = userInfo.Email,
                            PictureUrl = userInfo.Picture.Data.Url,
                            Provider = "Facebook",
                            DateCreated = DateTime.UtcNow
                        };

                        var createUser = await _userManager.CreateAsync(appUser);

                        if (createUser.Succeeded)
                        {
                            return Json(new { hasError = false, Token = _jwt.GenerateToken(userInfo.Email, appUser) });
                        }
                        else
                        {
                            return Json(new { hasError = true, Message = createUser.Errors });
                        }

                    }
                    else
                    {
                        if (user.Provider == "Direct")
                        {
                            return Json(new { hasError = true, Message = "Please sign-in using your email and password." });
                        }
                        else
                        {
                            return Json(new { hasError = false, Token = _jwt.GenerateToken(userInfo.Email, user) });
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                return Json(new { hasError = true, Message = "Error" });
            }
        }
        public async Task<IActionResult> GoogleLogin(string code)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri($"https://www.googleapis.com/oauth2/v4/token?code={code}&client_id=104151055965-6e02edcc6qnbb97ibgaqul475qshrjtd.apps.googleusercontent.com&client_secret=UYj6wi-omePtBPxmGzsKAECQ&redirect_uri="+ environment +"/googleauth&grant_type=authorization_code");

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, httpClient.BaseAddress);

                HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(req);

                if (!httpResponseMessage.IsSuccessStatusCode)
                {
                    return Json(new { hasError = true, Message = "Invalid google token." });
                }

                httpResponseMessage.EnsureSuccessStatusCode();
                HttpContent httpContent = httpResponseMessage.Content;
                var responseString = await httpContent.ReadAsStringAsync();

                var resultData = JsonConvert.DeserializeObject<GoogleAppAccessToken>(responseString);

                var userAccessTokenValidationResponse = await httpClient.GetStringAsync($"https://www.googleapis.com/plus/v1/people/me/openIdConnect?access_token={resultData.AccessToken}");

                var userInfo = JsonConvert.DeserializeObject<GoogleUserData>(userAccessTokenValidationResponse);

                var user = await _userManager.FindByEmailAsync(userInfo.Email);

                if (user == null)
                {
                    var appUser = new UserModelIdentity
                    {
                        FirstName = userInfo.GivenName,
                        LastName = userInfo.FamilyName,
                        Email = userInfo.Email,
                        UserName = userInfo.Email,
                        PictureUrl = userInfo.Picture,
                        Provider = "Google",
                        DateCreated = DateTime.UtcNow
                    };

                    var createUser = await _userManager.CreateAsync(appUser);

                    if (createUser.Succeeded)
                    {
                        return Json(new { hasError = false, Token = _jwt.GenerateToken(userInfo.Email, appUser) });
                    }
                    else
                    {
                        return Json(new { hasError = true, Message = createUser.Errors });
                    }

                }
                else
                {
                    if (user.Provider == "Direct")
                    {
                        return Json(new { hasError = true, Message = "Please sign-in using your email and password." });
                    }
                    else
                    {
                        return Json(new { hasError = false, Token = _jwt.GenerateToken(userInfo.Email, user) });
                    }
                }
            }
        }
        [HttpGet]
        public async Task<bool> ValidateToken(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token) && token != "null")
                {
                    bool isValidated = _jwt.ValidateToken(token);

                    if (isValidated)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
