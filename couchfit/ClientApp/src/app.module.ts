import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModalModule } from 'ngx-bootstrap';
import { ModalService } from './app/services/modal.service';
import { VideoEventsService } from './app/services/video-events.service';
import { AuthModule } from './app/auth/auth.module';
import { StartupModule } from './app/startup/startup.module';
import { AuthGuard } from './app/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    StartupModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    AuthModule
  ],
  bootstrap: [AppComponent],
  providers: [ModalService, VideoEventsService, AuthGuard]
})
export class AppModule { }