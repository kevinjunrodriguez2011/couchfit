﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.DatabaseModels
{
    public class UserSubscriptionModel
    {
        [Key]
        public int id { get; set; }
        public string user_id { get; set; }
        public int subscription_plan_id { get; set; }
        public string subscription_status { get; set; }
        public string subscription_type { get; set; }
        public string paypal_agreement_id { get; set; }
        public string paypal_payment_link { get; set; }
        public string stripe_cus_id { get; set; }
        public string stripe_plan_id { get; set; }
        public string stripe_sub_id { get; set; }
        public DateTime date_subscribed { get; set; }
        public DateTime date_suspended { get; set; }
        public DateTime date_cancelled { get; set; }
        public DateTime date_reactivated { get; set; }
        public string cancelled_reason { get; set; }
        public string suspended_reason { get; set; }
    }
}
