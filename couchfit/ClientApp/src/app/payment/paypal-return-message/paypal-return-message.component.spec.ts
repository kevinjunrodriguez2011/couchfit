import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaypalReturnMessageComponent } from './paypal-return-message.component';

describe('PaypalReturnMessageComponent', () => {
  let component: PaypalReturnMessageComponent;
  let fixture: ComponentFixture<PaypalReturnMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaypalReturnMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaypalReturnMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
