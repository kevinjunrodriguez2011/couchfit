﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.PaymentModels
{
    public class Payer
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public PlanType ChosenPlan { get; set; }
    }
}
