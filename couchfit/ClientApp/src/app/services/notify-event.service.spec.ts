import { TestBed, inject } from '@angular/core/testing';

import { NotifyEventService } from './notify-event.service';

describe('NotifyEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotifyEventService]
    });
  });

  it('should be created', inject([NotifyEventService], (service: NotifyEventService) => {
    expect(service).toBeTruthy();
  }));
});
