﻿using Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface
{
    public interface ISubscriptionPlanRepository
    {
        IReadOnlyCollection<SubscriptionPlansModel> GetSubscriptionPlans();
    }
}
