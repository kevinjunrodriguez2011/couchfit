﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Models.DatabaseModels;

namespace Core.Identity
{
    public class UserStore : IUserStore<UserModelIdentity>, IUserPasswordStore<UserModelIdentity>, IUserEmailStore<UserModelIdentity>
    {
        private readonly DatabaseFactory.AppContext _dbContext;
        public UserStore(DatabaseFactory.AppContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IdentityResult> CreateAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            if (user != null)
            {
                _dbContext.user.Add(user);
                _dbContext.SaveChanges();
                return await Task.FromResult(IdentityResult.Success);
            }
            return null;
        }

        public async Task<IdentityResult> DeleteAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            if (user != null)
            {
                int i = await _dbContext.SaveChangesAsync(cancellationToken);
                return await Task.FromResult(i == 1 ? IdentityResult.Success : IdentityResult.Failed());
            }
            return null;
        }
        public async Task<UserModelIdentity> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                var user = _dbContext.user.Where(usr => usr.Id.ToLower().Trim() == userId.ToLower().Trim()).FirstOrDefault();
                return await Task.FromResult(user);
            }
            return null;
        }

        public async Task<UserModelIdentity> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            var result = _dbContext.user.Where(usr => usr.UserName.Equals(normalizedUserName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return await Task.FromResult(result);
        }
        public Task<string> GetNormalizedUserNameAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<string> GetUserIdAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task<string> GetUserNameAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task<bool> HasPasswordAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task SetNormalizedUserNameAsync(UserModelIdentity user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.FromResult((object)null);
        }

        public Task SetPasswordHashAsync(UserModelIdentity user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;

            return Task.FromResult((object)null);
        }

        public Task SetUserNameAsync(UserModelIdentity user, string userName, CancellationToken cancellationToken)
        {
            user.UserName = userName;
            return Task.FromResult(user);
        }

        public Task<IdentityResult> UpdateAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        public Task SetEmailAsync(UserModelIdentity user, string email, CancellationToken cancellationToken)
        {
            user.Email = email;
            return Task.FromResult(user);
        }

        public Task<string> GetEmailAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(UserModelIdentity user, bool confirmed, CancellationToken cancellationToken)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(user);
        }

        public async Task<UserModelIdentity> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            var result = _dbContext.user.Where(usr => usr.Email.Equals(normalizedEmail, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return await Task.FromResult(result);
        }

        public Task<string> GetNormalizedEmailAsync(UserModelIdentity user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Email.Trim().ToLower());
        }

        public Task SetNormalizedEmailAsync(UserModelIdentity user, string normalizedEmail, CancellationToken cancellationToken)
        {
            user.Email = normalizedEmail.Trim().ToLower();
            return Task.FromResult(user);
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }

    }
}
