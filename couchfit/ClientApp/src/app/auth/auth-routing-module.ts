import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ConfirmEmailMessageComponent } from './confirm-email-message/confirm-email-message.component';
import { FacebookLoginComponent } from './facebook-login/facebook-login.component';
import { GoogleLoginComponent } from './google-login/google-login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'Auth/ConfirmEmail',
    component: ConfirmEmailComponent
  }, 
  {
    path: 'confirmemail',
    component: ConfirmEmailMessageComponent
  },
  {
    path: 'facebookauth',
    component: FacebookLoginComponent
  },
  {
    path: 'googleauth',
    component: GoogleLoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }