import { Component, OnInit, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';
import { UserService } from '../../services/user.service';
import { PlanTypeModel } from '../../models/payment/plan-type.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { PayerModel } from '../../models/payment/payer.model';
import { Subscription } from 'rxjs/Subscription';
import { PaymentService } from '../../services/payment.service';

@Component({
  selector: 'app-paypal-confirm-payment',
  templateUrl: './paypal-confirm-payment.component.html',
  styleUrls: ['./paypal-confirm-payment.component.css'],
  animations: [
    trigger('slidePay', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms', style({transform: 'translateX(100%)'}))
      ])
    ]),

    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class PaypalConfirmPaymentComponent implements OnInit {

  selectedPlan : PlanTypeModel;
  redirectStatus : string;
  paymentSubscription : Subscription;

  constructor(private router: Router, 
    private spinnerService: NgxSpinnerService,
    private userService: UserService,
    private paymentService: PaymentService) {
    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan')) as PlanTypeModel;
   }

  ngOnInit() {
  }
  backToPaymentSelection() {
    this.router.navigate(['onboard/subscribe'])
  }
  payWithPayPal() {
    this.spinnerService.show();
    this.redirectStatus = "Gathering data..."
    this.userService.getUserDetails().subscribe(user => {
      if (user != null) {
        this.redirectStatus = "Processing..."
        var payer = new PayerModel();
        payer.UserId = user.id;
        payer.FirstName = user.firstName;
        payer.LastName = user.lastName;
        payer.Email = user.email;
        payer.ChosenPlan = this.selectedPlan;
        this.paymentSubscription = this.paymentService.paypalCreateBillingAgreement(payer)
        .subscribe(paypalResponse => {
          this.redirectStatus = "Redirecting to Paypal..."
          window.location.href = paypalResponse.paypalBillingLink;
          setTimeout(() => {
            this.spinnerService.hide();
          }, 10000);
          this.paymentSubscription.unsubscribe();
        })
      }
    });
  }
}