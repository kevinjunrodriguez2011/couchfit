import { Component, OnInit } from '@angular/core';
import { SubscriptionPlanService } from '../../services/subscription-plan.service';
import { trigger, transition, animate, style } from '@angular/animations';
import { PlanTypeModel } from '../../models/payment/plan-type.model';
import { UserService } from '../../services/user.service';
import { NotifyEventService } from '../../services/notify-event.service';
import { Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-intro-plan',
  templateUrl: './intro-plan.component.html',
  styleUrls: ['./intro-plan.component.css'],
  animations: [
    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class IntroPlanComponent implements OnInit {

  planTypeList = new Array<PlanTypeModel>();

  constructor(private subscriptionPlanService: SubscriptionPlanService, 
    private userService: UserService, 
    private notifyEventService: NotifyEventService, 
    private router: Router,
    private sharedService: SharedService,
    private spinnerService: NgxSpinnerService) { 
    this.subscriptionPlanService.getSubscriptionPlans().subscribe(plans => {
      plans.forEach(element => {
        let planType = new PlanTypeModel();
        planType.PlanName = element.plan_name;
        planType.PlanPrice = element.plan_price;
        this.planTypeList.push(planType);
      });
    })
  }
  selectPlan(plan: PlanTypeModel) {
    localStorage.setItem('selectedPlan', JSON.stringify(plan))
    this.router.navigate(['/onboard/subscribe']);
  }
  skipPlanSelection() {
    this.spinnerService.show();
    this.sharedService.skipIntroPayment();
  }
  ngOnInit() {

  }
}