import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';
import { PayerModel } from '../models/payment/payer.model';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class UserSubscriptionService {

  constructor(private httpClient: HttpClient) { }

  getUserSubscription(userId: string) {
    let headers = new Headers();
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/usersubscription/GetUserSubscription?userId=' + userId, httpOptions)
  }
}
