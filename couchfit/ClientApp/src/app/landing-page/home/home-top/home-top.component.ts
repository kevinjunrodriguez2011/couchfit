import { Component, OnInit } from '@angular/core';
import { StartupService } from '../../../startup/startup.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-home-top',
  templateUrl: './home-top.component.html',
  styleUrls: ['./home-top.component.css']
})
export class HomeTopComponent implements OnInit {

  constructor(public startupService: StartupService, private router: Router) { }

  ngOnInit() {
    $(document).ready(function() {
    	setTimeout(function() {
    		$(".main").removeClass("is-loading");
    	}, 100)
    });
  }
  join() {
    this.router.navigate(['/register'])
  }
  openWebPlayer() {
    this.router.navigate(['/videos'])
  }
}
