import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaypalConfirmPaymentComponent } from './paypal-confirm-payment.component';

describe('PaypalConfirmPaymentComponent', () => {
  let component: PaypalConfirmPaymentComponent;
  let fixture: ComponentFixture<PaypalConfirmPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaypalConfirmPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaypalConfirmPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
