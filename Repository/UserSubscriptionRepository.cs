﻿using Interface;
using Models.DatabaseModels;
using System.Linq;
using Models.ViewModels;
using Models.PaymentModels;

namespace Repository
{
    public class UserSubscriptionRepository : IUserSubscriptionRepository
    {
        private readonly DatabaseFactory.AppContext _dbContext;
        public UserSubscriptionRepository(DatabaseFactory.AppContext dbContext)
        {
            _dbContext = dbContext;
        }

        public UserSubscriptionPlanViewModel GetUserSubscriptionPlan(string userId)
        {
            if (!string.IsNullOrWhiteSpace(userId))
            {
                var userSubscriptionPlan = _dbContext.user_subscription
                    .Where(usr => usr.user_id == userId).FirstOrDefault();
                var subscriptionPlan = _dbContext.subscription_plans.Where(plan => plan.id == userSubscriptionPlan.subscription_plan_id).FirstOrDefault();
                var userSubscriptionPlanViewModel = new UserSubscriptionPlanViewModel {
                    subscriptionPlansModel = subscriptionPlan,
                    SubscriptionStatus = userSubscriptionPlan.subscription_status
                };
                return userSubscriptionPlanViewModel;
            }
            return null;
        }

        public int SaveUserSubscription(UserSubscriptionViewModel userSubscriptionViewModel)
        {
            if (userSubscriptionViewModel != null)
            {
                var planType = _dbContext.subscription_plans.Where(plan => plan.plan_name == userSubscriptionViewModel.planType.PlanName).FirstOrDefault();
                userSubscriptionViewModel.userSubscriptionModel.subscription_plan_id = planType.id;
                _dbContext.user_subscription.Add(userSubscriptionViewModel.userSubscriptionModel);
               return _dbContext.SaveChanges();
            }
            return 0;
        }
        public int UpdateUserSubscription(UserSubscriptionViewModel userSubscriptionViewModel)
        {
            if (userSubscriptionViewModel != null)
            {
                var userSubscription = _dbContext.user_subscription.Where(usrSub => usrSub.user_id == userSubscriptionViewModel.userSubscriptionModel.user_id && usrSub.subscription_plan_id == userSubscriptionViewModel.planType.PlanId).FirstOrDefault();
                var user = _dbContext.user.Where(usr => usr.Id == userSubscriptionViewModel.userSubscriptionModel.user_id).FirstOrDefault();
                //if (user != null)
                //{
                //    user.SkippedPaymentIntro = true;
                //    _dbContext.user.Update(user);
                //}
                if (userSubscription != null)
                {
                    userSubscription.paypal_agreement_id = userSubscriptionViewModel.userSubscriptionModel.paypal_agreement_id;
                    userSubscription.date_subscribed = userSubscriptionViewModel.userSubscriptionModel.date_subscribed;
                    userSubscription.subscription_status = userSubscriptionViewModel.userSubscriptionModel.subscription_status;
                    _dbContext.user_subscription.Update(userSubscription);
                }
                return _dbContext.SaveChanges();
            }
            return 0;
        }
    }
}
