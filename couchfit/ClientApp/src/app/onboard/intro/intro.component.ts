import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { UserDetailsModel } from '../../models/user-details.model';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css'],
  animations: [
    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class IntroComponent implements OnInit {

  userDetailsModel = new UserDetailsModel();
  validationError = false;
  userDetails : any;

  constructor(private userService: UserService, private router: Router) {
    this.userDetailsModel.FirstName = '';
    this.userDetailsModel.LastName = '';
    this.userDetailsModel.Gender = '';


    this.userService.getUserDetails().subscribe(user => {
      this.userDetailsModel.FirstName = user.firstName === null ? '' : user.firstName;
      this.userDetailsModel.LastName = user.lastName === null ? '' : user.lastName;
      this.userDetailsModel.Gender = user.gender === null ? '' : user.gender;
      this.userDetailsModel.UserId = user.id;
    })
   }

  ngOnInit() {
  }
  updateUserDetail() {
    if (this.userDetailsModel.FirstName.trim() === '' || this.userDetailsModel.LastName.trim() === '' || this.userDetailsModel.Gender.trim() === '') {
      this.validationError = true;
      return;
    }
    else {
      if (this.userDetailsModel.UserId !== null) {
        this.validationError = false;
        this.userService.updateUserDetails({
          UserId: this.userDetailsModel.UserId,
          FirstName: this.userDetailsModel.FirstName,
          LastName: this.userDetailsModel.LastName,
          Gender: this.userDetailsModel.Gender
        }).subscribe(res => {
          if (!res.hasError) {
            localStorage.setItem('token', res.token);
            this.router.navigate(['/onboard/plans'])
          }
          else {
            alert('an error has occured! please contact support');
          }
        })
      }
    }
  }
}