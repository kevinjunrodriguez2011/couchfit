import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';
import { PayerModel } from '../models/payment/payer.model';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionPlanService {

  constructor(private httpClient: HttpClient) { 

  }
  getSubscriptionPlans() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.httpClient.get<any>('api/SubscriptionPlan/GetSubscriptionPlans', httpOptions)
  }
}
