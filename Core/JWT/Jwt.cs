﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Models.DatabaseModels;
using System.Security.Claims;
using Newtonsoft.Json;

namespace Core.JWT
{
    public class Jwt
    {
        private readonly IConfiguration _configuration;
        public Jwt(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public object GenerateToken(string email, UserModelIdentity user)
        {
            try
            {
                var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim("Status", "Authorized"),
                    new Claim("User", JsonConvert.SerializeObject(user))
                };
                var jwtSection = _configuration.GetSection("JWT");
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection["JwtKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var expires = DateTime.Now.AddDays(Convert.ToDouble(jwtSection["JwtExpireDays"]));

                var token = new JwtSecurityToken(
                    jwtSection["JwtIssuer"],
                    jwtSection["JwtIssuer"],
                    claims,
                    expires: expires,
                    signingCredentials: creds
                );

                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool ValidateToken(string token)
        {
            SecurityToken validatedToken;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtSection = _configuration.GetSection("JWT");
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(jwtSection["JwtExpireDays"]));
            TokenValidationParameters validationParameters =
            new TokenValidationParameters
            {
                ValidIssuer = jwtSection["JwtIssuer"],
                ValidAudience = jwtSection["JwtIssuer"],
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection["JwtKey"])),
            };
            ClaimsPrincipal user = handler.ValidateToken(token, validationParameters, out validatedToken);
            if (user != null) {
                return user.Identity.IsAuthenticated;
            }
            return false;
        }
        public IEnumerable<Claim> GetUserClaim(string token)
        {
            SecurityToken validatedToken;
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            var jwtSection = _configuration.GetSection("JWT");
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(jwtSection["JwtExpireDays"]));
            TokenValidationParameters validationParameters =
            new TokenValidationParameters
            {
                ValidIssuer = jwtSection["JwtIssuer"],
                ValidAudience = jwtSection["JwtIssuer"],
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSection["JwtKey"])),
            };
            ClaimsPrincipal user = handler.ValidateToken(token, validationParameters, out validatedToken);
            if (user != null)
            {
                return user.Claims;
            }
            return null;
        }
    }
}
