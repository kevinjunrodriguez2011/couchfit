﻿using Interface;
using System;
using PayPal.Core;
using PayPal.v1.Payments;
using System.Collections.Generic;
using System.Linq;
using PayPal.v1.BillingAgreements;
using PayPal.v1.BillingPlans;
using Models.PaymentModels;
using Models.DatabaseModels;
using Models.ViewModels;

namespace Services
{
    public class PaypalService : PaypalServiceBase, IPaypalService
    {
        private readonly string returnUrl = "http://couchfit.ca/payment/paypal/return";
        private readonly string cancelUrl = "http://couchfit.ca/payment/paypal/cancel";
        //private readonly string returnUrl = "http://localhost:49605/payment/paypal/return";
        //private readonly string cancelUrl = "http://localhost:49605/payment/paypal/cancel";
        public Plan CreateBillingPlan(PlanType planType)
        {
            string planPrice = "";

            switch (planType.PlanName)
            {
                case "Student":
                    planPrice = PlanPriceConst.Student.ToString();
                    break;
                case "Individual":
                    planPrice = PlanPriceConst.Individual.ToString();
                    break;
                case "Group":
                    planPrice = PlanPriceConst.Group.ToString();
                    break;
            }
            var plans = new PayPal.v1.BillingPlans.Plan()
            {
                Name = "Couchfit subscription plans",
                Description = "Couchfit billing plan for " + planType.PlanName,
                Type = "INFINITE",
                MerchantPreferences = new PayPal.v1.BillingPlans.MerchantPreferences()
                {
                    SetupFee = new PayPal.v1.BillingPlans.Currency() {
                        Value = "0.25",
                        CurrencyCode = "USD"
                    },
                    ReturnUrl = returnUrl,
                    CancelUrl = cancelUrl,
                    AutoBillAmount = "YES",
                    InitialFailAmountAction = "CONTINUE",
                    MaxFailAttempts = "3"
                },
                PaymentDefinitions = new List<PayPal.v1.BillingPlans.PaymentDefinition>()
                {
                    new PayPal.v1.BillingPlans.PaymentDefinition()
                    {
                        Name = "Couchfit " + planType.PlanName + " plan",
                        Frequency = "DAY",
                        FrequencyInterval = "1",
                        Cycles = "0",
                        Type = "REGULAR",
                        Amount = new PayPal.v1.BillingPlans.Currency()
                        {
                            Value = planPrice,
                            CurrencyCode = "USD"
                        },
                        ChargeModels = new List<ChargeModel>
                        {
                            new ChargeModel()
                            {
                                Type = "TAX",
                                Amount = new PayPal.v1.BillingPlans.Currency()
                                {
                                    Value = "0.55",
                                    CurrencyCode = "USD"
                                }
                            }
                        }
                    }
                }
            };

            PlanCreateRequest planCreateRequest = new PlanCreateRequest();
            planCreateRequest.RequestBody(plans);
            BraintreeHttp.HttpResponse response = this.ExecuteRequest(planCreateRequest);
            Plan createdPlan = response.Result<Plan>();

            return createdPlan;
        }
        public string CreateBillingAgreement(Models.PaymentModels.Payer payer)
        {
            string planPrice = "";

            switch (payer.ChosenPlan.PlanName)
            {
                case "Student":
                    planPrice = PlanPriceConst.Student.ToString();
                    break;
                case "Individual":
                    planPrice = PlanPriceConst.Individual.ToString();
                    break;
                case "Group":
                    planPrice = PlanPriceConst.Group.ToString();
                    break;
            }

            var createdPlan = this.CreateBillingPlan(new PlanType {
                PlanName = payer.ChosenPlan.PlanName,
            });

            var patchRequest = new List<PayPal.v1.BillingPlans.JsonPatch<Plan>>()
            {
                new PayPal.v1.BillingPlans.JsonPatch<Plan>()
                {
                    Op = "replace",
                    Path = "/",
                    Value = new Plan() { State = "ACTIVE" }
                }
            };

            PayPal.v1.BillingPlans.PlanUpdateRequest<Plan> planUpdateReq = new PayPal.v1.BillingPlans.PlanUpdateRequest<Plan>(createdPlan.Id);

            planUpdateReq.RequestBody(patchRequest);

            BraintreeHttp.HttpResponse planUpdateResp = this.ExecuteRequest(planUpdateReq);
            var billing = new PayPal.v1.BillingAgreements.Agreement()
            {
                Name = "Couchfit " + payer.ChosenPlan.PlanName + " plan subscription",
                Description = "Couchfit " + payer.ChosenPlan.PlanName + " subscription every month for $" + planPrice,
                StartDate = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-ddTHH:mm:ssZ"),
                Payer = new PayPal.v1.BillingAgreements.Payer()
                {
                   PaymentMethod = "Paypal",
                   PayerInfo = new PayPal.v1.BillingAgreements.PayerInformation()
                   {
                       FirstName = payer.FirstName,
                       LastName = payer.LastName,
                       Email = payer.Email,
                   },
                },
                Plan = new PlanWithId()
                {
                    Id = createdPlan.Id,
                },
                ShippingAddress = new SimplePostalAddress()
                {
                    Line1 = "111 Street",
                    City = "Ontario",
                    PostalCode = "34334",
                    State = "ON",
                    CountryCode = "CA",
                }
            };

            AgreementCreateRequest agreementRequest = new AgreementCreateRequest();
            agreementRequest.RequestBody(billing);

            System.Net.HttpStatusCode statusCode;

            try
            {
                BraintreeHttp.HttpResponse billingAgreementResp = this.ExecuteRequest(agreementRequest);

                statusCode = billingAgreementResp.StatusCode;

                Agreement result = billingAgreementResp.Result<Agreement>();
                
                string redirectUrl = null;
                foreach (var link in result.Links)
                {
                    if (link.Rel.Equals("approval_url"))
                    {
                        redirectUrl = link.Href;

                    }
                }
                return redirectUrl;

            }
            catch (BraintreeHttp.HttpException ex)
            {
                statusCode = ex.StatusCode;
                var debugId = ex.Headers.GetValues("PayPal-Debug-Id").FirstOrDefault();
                return "";
            }
        }
        public Agreement ExecuteBillingAgreement(UserPaymentViewModel userSubsriptionViewModel)
        {
            AgreementExecuteRequest agreementExecute = new AgreementExecuteRequest(userSubsriptionViewModel.PaypalPaymentToken);
            agreementExecute.Body = "[{}]";
            BraintreeHttp.HttpResponse agreementExecuteResp = this.ExecuteRequest(agreementExecute);
            Agreement result = agreementExecuteResp.Result<Agreement>();
            return result;
        }
        public Agreement CancelBillingAgreement(UserPaymentViewModel userSubsriptionViewModel)
        {
            PayPal.v1.BillingAgreements.AgreementCancelRequest agreementCancelRequest = new AgreementCancelRequest(userSubsriptionViewModel.AgreementId);
            agreementCancelRequest.Body = "[{}]";
            BraintreeHttp.HttpResponse agreementCancelResp = this.ExecuteRequest(agreementCancelRequest);
            Agreement result = agreementCancelResp.Result<Agreement>();
            return result;
        }
        public Agreement SuspendBillingAgreement(UserPaymentViewModel userSubsriptionViewModel)
        {
            AgreementSuspendRequest suspendAgreementRequest = new AgreementSuspendRequest(userSubsriptionViewModel.AgreementId);
            suspendAgreementRequest.Body = "[{}]";
            BraintreeHttp.HttpResponse suspendAgreementResp = this.ExecuteRequest(suspendAgreementRequest);
            Agreement result = suspendAgreementResp.Result<Agreement>();
            return result;
        }
        public Agreement ReactivateBillingAgreement(UserPaymentViewModel userSubsriptionViewModel)
        {
            AgreementReActivateRequest reactivateAgreementRequest = new AgreementReActivateRequest(userSubsriptionViewModel.AgreementId);
            reactivateAgreementRequest.Body = "[{}]";
            BraintreeHttp.HttpResponse reactivateAgreementResp = this.ExecuteRequest(reactivateAgreementRequest);
            Agreement result = reactivateAgreementResp.Result<Agreement>();
            return result;
        }
        public Agreement SuspendAgreement(string agreementId)
        {
            throw new NotImplementedException();
        }

    }
}
