﻿using Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public class EmailHelper : IEmailHelper
    {
        public async Task SendEmailAsync(string to, string body, string subject)
        {
            MailMessage email = new MailMessage();

            email.From = new MailAddress("couchfit.to@gmail.com", "Couchfit");
            email.To.Add(to);
            email.Subject = subject;
            email.IsBodyHtml = true;
            email.Body = body;

            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Credentials = new NetworkCredential("couchfit.to@gmail.com", "thisisforcouch");
                smtpClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                await smtpClient.SendMailAsync(email);
            }
        }
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                // prompt user with "send cancelled" message 
            }
            if (e.Error != null)
            {
                // prompt user with error message 
            }
            else
            {
                // prompt user with message sent!
                // as we have the message object we can also display who the message
                // was sent to etc 
            }
        }
    }
}
