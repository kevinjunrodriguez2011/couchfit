import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Endpoint } from '../../services/endpoint';

@Component({
  selector: 'app-facebook-login',
  templateUrl: './facebook-login.component.html',
  styleUrls: ['./facebook-login.component.css']
})
export class FacebookLoginComponent implements OnInit {

  private authWindow: Window;
  failed: boolean;
  error: string;
  errorDescription: string;
  isRequesting: boolean;

  launchFbLogin() {
     window.open('https://www.facebook.com/v3.11/dialog/oauth?&response_type=token&client_id=2228201457416269&display=popup&redirect_uri='+ Endpoint.currentEndPoint +'facebookauth&scope=email', null, 'width=600,height=400');
  }

  constructor(private authService: AuthService, private router: Router, private activatedRoute: ActivatedRoute) { 
    if (window.addEventListener) {
      window.addEventListener("message", this.handleMessage.bind(this), false);
    } else {
      (<any>window).attachEvent("onmessage", this.handleMessage.bind(this));
    }
  }
  handleMessage(event: Event) {
    const message = event as MessageEvent;
    // Only trust messages from the below origin.
    if (message.origin !== Endpoint.currentEndPoint) return;

    let windowUrl = window.location.href.split("#");
    let accessToken = '';

    if (window.location.href !== Endpoint.currentEndPoint + "/auth/login" && windowUrl[1] !== null) {
      accessToken = windowUrl[1].split("&")[0].replace("access_token=", "");
    }

    if (accessToken !== '') {
      this.authService.facebookLogin(accessToken).subscribe(
        result => {
          if (!result.hasError) {
            window.close();
            localStorage.setItem('token', result.token);
            window.opener.location = '/main';
          }
          if (result.hasError) {
            alert(result.message);
            window.close();
          }
        },
        error => {
          this.error = error;
        });
    }
  }
  ngOnInit() {
    this.authService.facebookLoginEventsSource$.subscribe(() => {
      this.launchFbLogin();
    })
  }
}
