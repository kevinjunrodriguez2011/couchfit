import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import { Subscription } from 'rxjs/Subscription';
import { VideoModel } from '../models/video-model';
import 'rxjs/add/operator/catch';

@Injectable()
export class VideoService {
  public videoList: any;
  
  constructor(private httpClient: HttpClient) { 
  }
  
  public getVideos(): Observable<any> {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/videos/GetVideos', httpOptions)
  }
  public getVideosCategories(): Observable<any> {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/videos/GetVideosCategory', httpOptions)
  }
  public getVideosByCategory(categoryId: number): Observable<any> {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/videos/GetVideosByCategory' + categoryId, httpOptions)
  }
  public getVideosById(videoId: number): Observable<VideoModel> {
    let token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization':  `Bearer ${token}`
      })
    };
    return this.httpClient.get<any>('api/videos/GetVideosById' + videoId, httpOptions)
  }
}