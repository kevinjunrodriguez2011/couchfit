import { Component, OnInit } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { SharedService } from '../../services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-paypal-cancel',
  templateUrl: './paypal-cancel.component.html',
  styleUrls: ['./paypal-cancel.component.css'],
  animations: [
    trigger('fadeBack', [
      transition(':enter', [
        style({ opacity:0 }),
        animate('1000ms ease-in-out', style({ opacity:1 }))
      ])
    ])
  ]
})
export class PaypalCancelComponent implements OnInit {

  constructor(private router: Router, private userService: UserService,private spinnerService: NgxSpinnerService,
    private sharedService: SharedService) { 

  }
  retrySubscription() {
    this.router.navigate(['onboard/plans'])
  }
  ngOnInit() {
  }
  skipIntroPayment() {
    this.spinnerService.show();
    this.sharedService.skipIntroPayment();
  }
}
