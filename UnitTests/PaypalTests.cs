using Interface;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Services;

namespace UnitTests
{
    [TestClass]
    public class PaypalTests
    {
        private readonly IPaypalService _IPaypalService;

        public PaypalTests()
        {
            var services = new ServiceCollection();
            services.AddTransient<IPaypalService, PaypalService>();
            var serviceProvider = services.BuildServiceProvider();
            _IPaypalService = serviceProvider.GetService<IPaypalService>();
        }

        [TestMethod]
        public void PaypalInitTest()
        {
            //_IPaypalService.ExecuteBillingAgreement("");

        }
    }
}
