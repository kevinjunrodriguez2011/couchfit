import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroComponent } from './intro/intro.component';
import { IntroPaymentComponent } from './intro-payment/intro-payment.component';
import { IntroGuard } from '../intro.guard';
import { IntroPaymentGuard }  from '../intro-payment.guard';
import { IntroPlanComponent } from './intro-plan/intro-plan.component';
import { PaypalConfirmPaymentComponent } from './paypal-confirm-payment/paypal-confirm-payment.component';
import { StripeConfirmPaymentComponent } from './stripe-confirm-payment/stripe-confirm-payment.component';

const routes: Routes = [
  {
    path: 'welcome',
    component: IntroComponent,
    canActivate: [IntroGuard]
  },
  {
    path: 'plans',
    component: IntroPlanComponent,
    canActivate: [IntroPaymentGuard]
  },
  {
    path: 'subscribe',
    component: IntroPaymentComponent,
    canActivate: [IntroPaymentGuard]
  }, 
  {
    path: 'payment-confirm/paypal',
    component: PaypalConfirmPaymentComponent,
    canActivate: [IntroPaymentGuard]
  },
  {
    path: 'payment-confirm/cc',
    component: StripeConfirmPaymentComponent,
    canActivate: [IntroPaymentGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardRoutingModule { }
