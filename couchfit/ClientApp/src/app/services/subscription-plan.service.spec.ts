import { TestBed, inject } from '@angular/core/testing';

import { SubscriptionPlanService } from './subscription-plan.service';

describe('SubscriptionPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscriptionPlanService]
    });
  });

  it('should be created', inject([SubscriptionPlanService], (service: SubscriptionPlanService) => {
    expect(service).toBeTruthy();
  }));
});
