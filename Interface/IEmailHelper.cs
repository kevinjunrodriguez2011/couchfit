﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface IEmailHelper
    {
        Task SendEmailAsync(string to, string body, string subject);
    }
}
