import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { PaymentService } from '../../services/payment.service';
import { UserService } from '../../services/user.service';
import { UserPaymentViewModel } from '../../models/ViewModels/UserPaymentViewModel';
import { PlanTypeModel } from '../../models/payment/plan-type.model';
import { Router, ActivatedRoute } from '@angular/router'
import { UserSubscriptionService } from '../../services/user-subscription.service';

@Component({
  selector: 'app-paypal-return',
  templateUrl: './paypal-return.component.html',
  styleUrls: ['./paypal-return.component.css']
})
export class PaypalReturnComponent implements OnInit {

  constructor(private sharedService: SharedService, 
    private paymentService: PaymentService, 
    private userService: UserService, 
    private userSubscriptionService: UserSubscriptionService,
    private router: Router, 
    private activatedRoute: ActivatedRoute) { 
    
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      let token = params['token'];
      let paymentToken = token;
      if (paymentToken != undefined) {
        this.userService.getUserDetails().subscribe(user => {
          this.userSubscriptionService.getUserSubscription(user.id).subscribe(usrSub => {
            var selectedPlan = usrSub.userSubscriptionPlan.subscriptionPlansModel as PlanTypeModel;
            var userPaymentViewModel = new UserPaymentViewModel();
            userPaymentViewModel.PaypalPaymentToken = paymentToken;
            userPaymentViewModel.UserId = user.id;
            userPaymentViewModel.SubscriptionPlansModel = selectedPlan;
            this.paymentService.paypalExecuteBillingAgremeent(userPaymentViewModel).subscribe(res => {
              if (res) {
                if (res.hasError == undefined) {
                  localStorage.setItem('returnSuccess', "yes");
                  this.router.navigate(['payment/paypal/return-message'])
                }
                else {
                  window.location.href = "/main";
                }
              }
            })
          })
        })
      }
    });
  }
}
