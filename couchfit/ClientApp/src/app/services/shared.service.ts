import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';


@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private userService: UserService) { }

  setLocalStorage(key: string, value: string) {
    localStorage.setItem(key, value)
  }
  getLocalStorage(key: string) {
    return localStorage.getItem(key)
  }
  skipIntroPayment() {
    this.userService.getUserDetails().subscribe(user => {
      this.userService.skipIntroPayment(user.id).subscribe((res) => {
        localStorage.setItem('token', res.token);
        window.location.href = "/videos";
      })
    });
  }
  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }
}
