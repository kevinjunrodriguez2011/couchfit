import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserDetailsService {

  userObj: any;

  constructor() { }

  getUserDetails() {
    return this.userObj;
  }

  setUserDetails(userObj) {
    this.userObj = userObj;
  }

}
