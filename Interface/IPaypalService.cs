﻿using Models.DatabaseModels;
using Models.PaymentModels;
using Models.ViewModels;
using PayPal.v1.BillingAgreements;
using PayPal.v1.BillingPlans;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interface
{
    public interface IPaypalService
    {
        string CreateBillingAgreement(Models.PaymentModels.Payer payer);
        Plan CreateBillingPlan(PlanType planType);
        Agreement ExecuteBillingAgreement(UserPaymentViewModel userSubsriptionViewModel);
        Agreement CancelBillingAgreement(UserPaymentViewModel userSubsriptionViewModel);
        Agreement SuspendBillingAgreement(UserPaymentViewModel userSubsriptionViewModel);
        Agreement ReactivateBillingAgreement(UserPaymentViewModel userSubsriptionViewModel);
    }
}
