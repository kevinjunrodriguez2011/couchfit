﻿using Models.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace DatabaseFactory
{
    public class AppContext : IdentityDbContext<UserModelIdentity>
    {
        public AppContext(DbContextOptions options) : base(options) {
        }
        public DbSet<VideoModel> videos { get; set; }
        public DbSet<CategoriesModel> videos_categories { get; set; }
        public DbSet<UserModelIdentity> user { get; set; }
        public DbSet<UserRoleModel> user_role { get; set; }
        public DbSet<RoleModel> role { get; set; }
        public DbSet<SubscriptionPlansModel>  subscription_plans { get; set; }
        public DbSet<UserSubscriptionModel> user_subscription { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserModelIdentity>(entity =>
            {
                entity.ToTable(name: "users");
            });
        }
    }
}
