import { TestBed, async, inject } from '@angular/core/testing';

import { IntroPaymentGuard } from './intro-payment.guard';

describe('IntroPaymentGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IntroPaymentGuard]
    });
  });

  it('should ...', inject([IntroPaymentGuard], (guard: IntroPaymentGuard) => {
    expect(guard).toBeTruthy();
  }));
});
